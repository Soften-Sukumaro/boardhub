const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const session = require('express-session');
const serveStatic = require('serve-static');
const fs = require('fs');

const api = require('./routes/api');

// Create Folder
// if (!fs.existsSync('./uploads')) {
//   fs.mkdirSync('./uploads');
// }
// if (!fs.existsSync('./uploads/products')) {
//   fs.mkdirSync('./uploads/products');
// }
// if (!fs.existsSync('./uploads/users')) {
//   fs.mkdirSync('./uploads/users');
// }

// init app
const app = express();

// Body Parser Middleware
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false,
}));

// parse application/json
app.use(bodyParser.json());

// Passport Config
require('./config/passport');

// Cookie Parser
app.use(cookieParser('gfjgiregjreiosgnreiognrepsgripeognreopgn'));

app.use(session({
  secret: 'gfjgiregjreiosgnreiognrepsgripeognreopgn',
  resave: true,
  saveUninitialized: true,
  cookie: {
    // httpOnly: true,
    secure: false,
    path: '/',
    maxAge: null,
  },
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

// CORS middleware
const allowCrossDomain = function f(req, res, next) {
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header(
    'Access-Control-Allow-Headers',
    'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept',
  );
  next();
};

app.use(allowCrossDomain);

// Route Files
// Set Public Folder
app.use('/uploads', express.static(path.join(__dirname, 'uploads'), {
  fallthrough: false,
}));

// Path /api
app.use('/api', api);

// create middleware to handle the serving the app
app.use('/', serveStatic(path.join(__dirname, '/dist')));

// Catch all routes and redirect to the index file
app.use('*', (req, res) => res.sendFile(`${__dirname}/dist/index.html`));

const port = process.env.PORT || 3000;
// Start Server
app.listen(port, () => { /* console.log('Server Listen At ', port); */ });
