const multer = require('multer');
const AWS = require('aws-sdk');
const multerS3 = require('multer-s3');

const awsS3 = new AWS.S3({
  accessKeyId: 'AKIAIFJMFN4QUIEFYTXQ',
  secretAccessKey: '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',
  region: 'ap-southeast-1',
});

exports.imageFilter = function f(req, file, cb) {
  if (!file.originalname.toLowerCase().match(/\.(jpg|jpeg|png|gif)$/)) {
    return cb(new Error('Only image files are allowed!'), false);
  }
  cb(null, true);
  return 0;
};

// Limit Image Size 10MB
exports.imageSize = 10 * 1024 * 1024;

exports.productStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads/products/');
  },
  filename(req, file, cb) {
    const ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
    cb(null, `${Date.now()}_${Math.floor(Math.random() * Math.floor(10000000000))}_${file.fieldname}${ext}`);
  },
});

exports.userStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads/users/');
  },
  filename(req, file, cb) {
    const ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
    cb(null, `${Date.now()}_${file.fieldname}${ext}`);
  },
});

exports.uploadS3product = multer({
  storage: multerS3({
    s3: awsS3,
    bucket: 'boardhubphotos',
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata(req, file, cb) {
      cb(null, {
        fieldName: file.fieldname,
      });
    },
    key(req, file, cb) {
      const ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
      cb(null, `${Date.now()}_${Math.floor(Math.random() * Math.floor(10000000000))}_product_${file.fieldname}${ext}`);
    },
  }),
  fileFilter: exports.imageFilter,
  limits: {
    fileSize: exports.imageSize,
  },
});

exports.uploadS3user = multer({
  storage: multerS3({
    s3: awsS3,
    bucket: 'boardhubphotos',
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata(req, file, cb) {
      cb(null, {
        fieldName: file.fieldname,
      });
    },
    key(req, file, cb) {
      const ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
      cb(null, `${Date.now()}_${Math.floor(Math.random() * Math.floor(10000000000))}_user_${file.fieldname}${ext}`);
    },
  }),
  fileFilter: exports.imageFilter,
  limits: {
    fileSize: exports.imageSize,
  },
});


exports.uploadS3confirmation = multer({
  storage: multerS3({
    s3: awsS3,
    bucket: 'boardhubphotos',
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata(req, file, cb) {
      cb(null, {
        fieldName: file.fieldname,
      });
    },
    key(req, file, cb) {
      const ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
      cb(null, `${Date.now()}_${Math.floor(Math.random() * Math.floor(10000000000))}_confirmation_${file.fieldname}${ext}`);
    },
  }),
  fileFilter: exports.imageFilter,
  limits: {
    fileSize: exports.imageSize,
  },
});
