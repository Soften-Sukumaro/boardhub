const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const {
  User,
} = require('./sequelize');

passport.use(new LocalStrategy(
  ((username, password, done) => {
    User.findOne({
      where: {
        username,
      },
    }).then((user) => {
      if (!user) {
        return done(null, false, {
          message: 'Incorrect username.',
        });
      }
      const passwordValid = bcrypt.compareSync(
        password, user.password,
      );
      if (!passwordValid) {
        return done(null, false, {
          message: 'Incorrect password.',
        });
      }
      const userinfo = user.get();
      return done(null, userinfo);
    }).catch(() => done(null, false, {
      message: 'Something went wrong with your Signin',
    }));
  }),
));

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id).then((user) => {
    if (user) {
      done(null, user.get());
    } else {
      done(user.errors, null);
    }
  });
});
