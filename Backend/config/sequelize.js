const Sequelize = require('sequelize');

const UserModel = require('../models/users');
const ProductModel = require('../models/products');
const ProductPhotoModel = require('../models/productPhotos');
const OrdersModel = require('../models/orders');
const OrderProductModel = require('../models/orderProduct');
const RefOrderProductStatusModel = require('../models/refOrderProductStatus');
const RefOrderStatusModel = require('../models/refOrderStatus');
const InvoiceModel = require('../models/invoice');
const RefInvoiceStatusModel = require('../models/refInvoiceStatus');
const ConfirmationModel = require('../models/confirmation');
const RefConfirmationStatusModel = require('../models/refConfirmationStatus');
const ShipmentModel = require('../models/shipment');
const ProductCommentModel = require('../models/productComments');
const RefShipmentStatusModel = require('../models/refShipmentStatus');

const dbName = process.env.NODE_ENV === 'production' ? 'Boardhubnew'
  : 'Boardhubnew';

const sequelize = new Sequelize(dbName, 'boardhubnew', '1234567890', {
  host: '35.185.184.251',
  dialect: 'mysql',
  pool: {
    max: 20,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  logging: true,
});

const User = UserModel(sequelize, Sequelize);
const Product = ProductModel(sequelize, Sequelize);
const ProductPhoto = ProductPhotoModel(sequelize, Sequelize);
const Order = OrdersModel(sequelize, Sequelize);
const OrderProduct = OrderProductModel(sequelize, Sequelize);
const RefOrderProductStatus = RefOrderProductStatusModel(sequelize, Sequelize);
const RefOrderStatus = RefOrderStatusModel(sequelize, Sequelize);
const Invoice = InvoiceModel(sequelize, Sequelize);
const RefInvoiceStatus = RefInvoiceStatusModel(sequelize, Sequelize);
const Confirmation = ConfirmationModel(sequelize, Sequelize);
const RefConfirmationStatus = RefConfirmationStatusModel(sequelize, Sequelize);
const Shipment = ShipmentModel(sequelize, Sequelize);
const ProductComment = ProductCommentModel(sequelize, Sequelize);
const RefShipmentStatus = RefShipmentStatusModel(sequelize, Sequelize);

// !!! Must give Relation at 2 side
ProductPhoto.Product = ProductPhoto.belongsTo(Product); // fk_productid = product.id
Product.ProductPhoto = Product.hasMany(ProductPhoto); // fk_productid = product.id

Order.User = Order.belongsTo(User, {
  onDelete: 'cascade',
  hooks: true,
});
User.Order = User.hasMany(Order, {
  onDelete: 'cascade',
  hooks: true,
});

Order.RefOrderStatus = Order.belongsTo(RefOrderStatus);
RefOrderStatus.Order = RefOrderStatus.hasMany(Order);

OrderProduct.Order = OrderProduct.belongsTo(Order, {
  onDelete: 'cascade',
  hooks: true,
});
Order.OrderProduct = Order.hasMany(OrderProduct, {
  onDelete: 'cascade',
  hooks: true,
});

OrderProduct.Product = OrderProduct.belongsTo(Product, {
  onDelete: 'cascade',
  hooks: true,
});
Product.OrderProduct = Product.hasMany(OrderProduct, {
  onDelete: 'cascade',
  hooks: true,
});

OrderProduct.RefOrderProductStatus = OrderProduct.belongsTo(RefOrderProductStatus);
RefOrderProductStatus.OrderProduct = RefOrderProductStatus.hasMany(OrderProduct);

Invoice.RefInvoiceStatus = Invoice.belongsTo(RefInvoiceStatus);
RefInvoiceStatus.Invoice = RefInvoiceStatus.hasMany(Invoice);

Invoice.Order = Invoice.belongsTo(Order);
Order.Invoice = Order.hasOne(Invoice);

Confirmation.Invoice = Confirmation.belongsTo(Invoice);
Invoice.Confirmation = Invoice.hasOne(Confirmation);

Confirmation.RefConfirmationStatus = Confirmation.belongsTo(RefConfirmationStatus);
RefConfirmationStatus.Confirmation = RefConfirmationStatus.hasMany(Confirmation);

Shipment.RefShipmentStatus = Shipment.belongsTo(RefShipmentStatus);
RefShipmentStatus.Shipment = RefShipmentStatus.hasMany(Shipment);

User.sendShipment = User.hasMany(Shipment, {
  onDelete: 'cascade',
  hooks: true,
  foreignKey: 'sender',
});
User.receiveShipment = User.hasMany(Shipment, {
  onDelete: 'cascade',
  hooks: true,
  foreignKey: 'receiver',
});

OrderProduct.Shipment = OrderProduct.belongsTo(Shipment, {
  onDelete: 'cascade',
  hooks: true,
});
Shipment.OrderProduct = Shipment.hasMany(OrderProduct, {
  onDelete: 'cascade',
  hooks: true,
});

Product.ProductComment = Product.hasMany(ProductComment);
ProductComment.Product = ProductComment.belongsTo(Product);


sequelize.sync({
  force: false,
})
  .then(() => {
    // console.log('Database & tables created!');
  });

module.exports = {
  User,
  Product,
  ProductPhoto,
  Order,
  OrderProduct,
  RefOrderStatus,
  RefOrderProductStatus,
  Invoice,
  RefInvoiceStatus,
  Confirmation,
  RefConfirmationStatus,
  Shipment,
  ProductComment,
  RefShipmentStatus,
};
