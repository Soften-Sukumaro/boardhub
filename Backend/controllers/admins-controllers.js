const bcrypt = require('bcryptjs');
const mail = require('./mail');
const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');
const {
  User,
} = require('../config/sequelize');

exports.register = asyncMiddleware(async (req, res, next) => {
  const today = new Date();
  const newUser = {
    first_name: req.body.firstname,
    last_name: req.body.lastname,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
    city: req.body.city,
    country: req.body.country,
    phone_number: req.body.phone,
    address: req.body.address,
    gender: req.body.gender,
    created: today,
    role: 'admin',
  };

  const salt = await bcrypt.genSalt(10);
  newUser.password = await bcrypt.hash(newUser.password, salt);
  await User.create(newUser).catch(err => res.status(400).send(err));
  res.status(201).send({
    auth: true,
    user: newUser.username,
    success: 'admin registered sucessfully',
    role: 'admin',
  });
  await mail.sendMailText(req.body.email, 'Boardhub Admin Register Seccess', 'Success');
});

exports.login = asyncMiddleware(async (req, res, next) => {
  if (req.user && req.user.role === 'admin') {
    const json = {
      id: req.user.id,
      username: req.user.username,
      role: req.user.role,
    };
    res.status(200).json(json);
  } else res.sendStatus(401);
});

exports.logout = asyncMiddleware(async (req, res, next) => {
  req.logout();
  res.sendStatus(200);
});

exports.changerole = asyncMiddleware(async (req, res, next) => {
  await User.update({
    role: req.body.role,
  }, {
    returning: true,
    where: {
      username: req.body.username,
    },
  });
  res.sendStatus(200);
});
