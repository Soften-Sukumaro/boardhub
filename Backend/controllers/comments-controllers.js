const {
  Product,
  ProductComment,
} = require('../config/sequelize');
const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');

exports.addComments = asyncMiddleware(async (req, res, next) => {
  //  const today = new Date();
  Product.findOne({
    where: {
      id: req.body.id,
    },
  }).then((result) => {
    if (!result) res.status(400).send(`Cannot find Product id:${req.body.id}`);
    else {
      ProductComment.create({
        comment: req.body.comment,
        productId: req.body.id,
        userId: req.user.id,
      }).then((r) => {
        // console.log(r.id);
        res.status(200).send(r);
      }).catch(err => res.status(400).send(err));
    }
  }).catch(err => res.status(400).send(err));
});

exports.deleteComments = asyncMiddleware(async (req, res, next) => {
  const r = await ProductComment.destroy({
    where: {
      id: req.params.id,
    },
  }).catch(err => res.status(400).send(err));
    // console.log(r);
  if (r) {
    res.sendStatus(200);
  } else {
    res.sendStatus(400);
  }
});

exports.changeComments = asyncMiddleware(async (req, res, next) => {
  ProductComment.update({
    comment: req.body.comment,
  }, {
    where: {
      id: req.body.id,
      userId: req.user.id,
    },
  }).then((r) => {
    res.status(200).send(r);
  }).catch(err => res.status(400).send(err));
});

exports.getComments = asyncMiddleware(async (req, res, next) => {
  //  const today = new Date();
  ProductComment.findOne({
    where: {
      id: req.params.id,
    },
  }).then((result) => {
    if (!result) res.status(400).send(`Cannot find Comment id:${req.body.id}`);
    else {
      res.status(200).send(result);
    }
  }).catch(err => res.status(400).send(err));
});
