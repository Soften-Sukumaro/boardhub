const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');

const {
  Order,
  OrderProduct,
  Confirmation,
  Invoice,
  RefConfirmationStatus,
} = require('../config/sequelize');

exports.add = asyncMiddleware(async (req, res) => {
  // console.log('add confirmation');
  const newConfirmation = {
    invoiceId: req.body.invoiceId,
    message: req.body.message,
    transfer_datetime: req.body.datetime,
    refConfirmationStatusId: 1,
    create_by: req.user.id,
    photo_link: req.body.photo,
  };
  Confirmation.create(newConfirmation).then((result) => {
    Invoice.update({
      refInvoiceStatusId: 2,
    }, {
      where: {
        id: req.body.invoiceId,
      },
    }).catch((err) => {
      res.status(400).send(err);
    }).then(() => {
      res.status(200).send(result);
    });
  }).catch((err) => {
    // console.log(err);
    res.status(400).send(err);
  });
});

exports.addphoto = asyncMiddleware(async (req, res) => {
  res.status(200).send({
    link: req.file.location,
  });
});

exports.confirm = asyncMiddleware(async (req, res) => {
  // console.log('admin confirm the confirmation');
  Confirmation.findOne({
    where: {
      id: req.body.id,
    },
  }).then((result) => {
    // console.log(result.id);
    Confirmation.update({
      refConfirmationStatusId: 2,
    }, {
      where: {
        id: result.id,
      },
    }).catch((err) => {
      res.status(400).send(err);
    }).catch((err) => {
      res.status(400).send(err);
    });
    Invoice.update({
      refInvoiceStatusId: 3,
    }, {
      where: {
        id: result.invoiceId,
      },
    }).catch((err) => {
      res.status(400).send(err);
    });
    Invoice.findOne({
      where: {
        id: result.invoiceId,
      },
    }).then((invoice) => {
      Order.update({
        refOrderStatusId: 3,
      }, {
        where: {
          id: invoice.orderId,
        },
      }).catch((err) => {
        res.status(400).send(err);
      });
      OrderProduct.update({
        refOrderProductStatusId: 3,
      }, {
        where: {
          orderId: invoice.orderId,
        }
      })
    });
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getUnconfirmByUser = asyncMiddleware(async (req, res) => {
  Confirmation.findAll({
    where: {
      refConfirmationStatusId: 1,
      create_by: req.params.id,
    },
    include: [{
      model: RefConfirmationStatus,
      attributes: ['description'],
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getConfirmedByUser = asyncMiddleware(async (req, res) => {
  Confirmation.findAll({
    where: {
      refConfirmationStatusId: 2,
      create_by: req.params.id,
    },
    include: [{
      model: RefConfirmationStatus,
      attributes: ['description'],
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getDetail = asyncMiddleware(async (req, res) => {
  Confirmation.findOne({
    where: {
      id: req.params.id,
    },
    include: [{
      model: RefConfirmationStatus,
      attributes: ['description'],
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getByOrder = asyncMiddleware(async (req, res) => {
  Confirmation.findOne({
    include: [{
      model: Invoice,
      where: {
        orderId: req.params.id,
      },
      attributes: ['id', 'orderId'],
    }, {
      model: RefConfirmationStatus,
      attributes: ['description'],
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getPicture = asyncMiddleware(async (req, res) => {
  Confirmation.findOne({
    where: {
      id: req.params.id,
    },
    attributes: ['photo_link'],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getAll = asyncMiddleware(async (req, res) => {
  Confirmation.findAll({
    include: [{
      model: RefConfirmationStatus,
      attributes: ['description'],
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});