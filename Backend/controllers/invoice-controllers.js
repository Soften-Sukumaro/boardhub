const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');
const {
  Order,
  Invoice,
  RefInvoiceStatus,
} = require('../config/sequelize');

exports.add = asyncMiddleware(async (req, res) => {
  // console.log('add Invoice');
  const newInvoice = {
    orderId: req.body.orderId,
    price: req.body.price,
    detail: req.body.detail,
    refInvoiceStatusId: 1,
  };
  Invoice.create(newInvoice).then((result) => {
    // console.log(result);
    res.status(200).send(result);
  }).catch((err) => {
    // console.log(err);
    res.status(400).send(err);
  });
});

exports.getDetail = asyncMiddleware(async (req, res) => {
  // console.log('get invoice by id');
  Invoice.findOne({
    where: {
      id: req.params.id,
    },
    include: [{
      model: RefInvoiceStatus,
      attributes: ['description'],
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getUnpaidByUser = asyncMiddleware(async (req, res) => {
  Invoice.findAll({
    include: [{
      model: Order,
      where: {
        userId: req.user.id,
        refOrderStatusId: 2,
      },
      attributes:[],
    }, {
      model: RefInvoiceStatus,
      attributes: ['description'],
    }]
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getByOrder = asyncMiddleware(async (req,res)  => {
  Invoice.findOne({
    where: {
      orderId: req.params.id,
    },
    include: {
      model: RefInvoiceStatus,
      attributes: ['description'],
    },
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  })
});