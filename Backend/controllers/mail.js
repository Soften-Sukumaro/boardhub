const nodemailer = require('nodemailer');

const boardhubMail = 'boardhub.project@gmail.com';
const boardhubMailPass = 'BoardhubBoardhub30';
const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: boardhubMail,
    pass: boardhubMailPass,
  },
});

exports.sendMailText = asyncMiddleware(async (to, subject, text) => {
  const mailOptions = {
    from: boardhubMail,
    to,
    subject,
    text,
  };

  const emailResponse = await transporter.sendMail(mailOptions).catch(err => console.log(err));
  return emailResponse;
});

exports.sendMailHtml = asyncMiddleware(async (to, subject, html) => {
  const mailOptions = {
    from: boardhubMail,
    to,
    subject,
    html,
  };

  const emailResponse = await transporter.sendMail(mailOptions).catch(err => console.log(err));
  return emailResponse;
});
