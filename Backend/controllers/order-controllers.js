const { asyncMiddleware } = require('../middlewares/asyncMiddleware');
const {
  Order,
  RefOrderStatus,
  Invoice,
  OrderProduct,
} = require('../config/sequelize');

exports.getAll = asyncMiddleware(async (req, res, next) => {
  Order.findAll({
    include: [
      {
        model: RefOrderStatus,
        attributes: ['description'],
      },
    ],
  })
    .then((result) => {
      // console.log(result);
      if (result === null) {
        res.status(400).send({
          failed: 'error ocurred',
        });
      } else {
        res.status(200).send(result);
      }
    })
    .catch((err) => {
      // console.log(err);
      res.status(400).send(err);
    });
});

exports.getByUserId = asyncMiddleware(async (req, res, next) => {
  if (req.params.id === 'me' || req.user.id === req.params.id) {
    // console.log(req.user);
    Order.findAll({
      where: {
        userId: req.user.id,
      },
      include: [
        {
          model: RefOrderStatus,
          attributes: ['description'],
        },
      ],
    })
      .then((result) => {
        if (result === null) {
          res.status(401).send({
            failed: 'order not found',
          });
        } else {
          res.status(200).send(result);
        }
      })
      .catch(err => res.status(400).send(err));
  } else {
    Order.findAll({
      where: {
        userId: req.params.id,
      },
      include: [
        {
          model: RefOrderStatus,
          attributes: ['description'],
        },
      ],
    })
      .then((result) => {
        //  console.log(result);
        if (result === null) {
          res.status(401).send({
            auth: false,
            failed: 'order not found',
          });
        } else {
          res.status(200).send(result);
        }
      })
      .catch(err => res.status(400).send(err));
  }
});

exports.getCart = asyncMiddleware(async (req, res, next) => {
  if (req.params.id === 'me' || req.user.id === req.params.id) {
    //  console.log(req.user);
    Order.findAll({
      where: {
        userId: req.user.id,
        refOrderStatusId: 1,
      },
      include: [
        {
          model: RefOrderStatus,
          attributes: ['description'],
        },
      ],
    })
      .then((result) => {
        if (result === null) {
          res.status(401).send({
            failed: 'order not found',
          });
        } else {
          res.status(200).send(result);
        }
      })
      .catch(err => res.status(400).send(err));
  } else {
    Order.findAll({
      where: {
        userId: req.params.id,
        refOrderStatusId: 1,
      },
      include: [
        {
          model: RefOrderStatus,
          attributes: ['description'],
        },
      ],
    })
      .then((result) => {
        // console.log(result);
        if (result === null) {
          res.status(401).send({
            auth: false,
            failed: 'order not found',
          });
        } else {
          res.status(200).send(result);
        }
      })
      .catch(err => res.status(400).send(err));
  }
});

exports.getNonCart = asyncMiddleware(async (req, res, next) => {
  if (req.params.id === 'me' || req.user.id === req.params.id) {
    //  console.log(req.user);
    Order.findAll({
      where: {
        userId: req.user.id,
        refOrderStatusId: 1,
      },
      include: [
        {
          model: RefOrderStatus,
          attributes: ['description'],
        },
      ],
    })
      .then((result) => {
        if (result === null) {
          res.status(401).send({
            failed: 'order not found',
          });
        } else {
          res.status(200).send(result);
        }
      })
      .catch(err => res.status(400).send(err));
  } else {
    Order.findAll({
      where: {
        userId: req.params.id,
        refOrderStatusId: 1,
      },
      include: [
        {
          model: RefOrderStatus,
          attributes: ['description'],
        },
      ],
    })
      .then((result) => {
        // console.log(result);
        if (result === null) {
          res.status(401).send({
            auth: false,
            failed: 'order not found',
          });
        } else {
          res.status(200).send(result);
        }
      })
      .catch(err => res.status(400).send(err));
  }
});

exports.getDetail = asyncMiddleware(async (req, res, next) => {
  Order.findOne({
    where: {
      id: req.params.id,
    },
    include: [
      {
        model: RefOrderStatus,
        attributes: ['description'],
      },
    ],
  })
    .then((result) => {
      //  console.log(result);
      if (result === null) {
        res.status(401).send({
          // auth: false,
          failed: 'order not found',
        });
      } else if (req.user.id === result.userId || req.user.role === 'admin') {
        res.status(200).send(result);
      } else {
        res.status(200).send({
          // auth: false,
          failed: 'not authorized to see detail',
        });
      }
    })
    .catch((err) => {
      // console.log(err);
      res.status(400).send(err);
    });
});

exports.checkout = asyncMiddleware(async (req, res, next) => {
  const today = new Date();
  Order.update(
    {
      refOrderStatusId: 2,
      date_order_place: today,
    },
    {
      returning: true,
      where: {
        id: req.body.id,
      },
    },
  )
    .then((rowUpdated) => {
      // console.log('update cart complete');
      OrderProduct.update(
        {
          refOrderProductStatusId: 2,
        },
        {
          returning: true,
          where: {
            orderId: req.body.id,
          },
        },
      )
        .then((rowUpdated) => {
          Invoice.create({
            orderId: req.body.id,
            refInvoiceStatusId: 1,
            price: req.body.price,
            detail: req.body.detail,
          })
            .then((result) => {
              // console.log('create invoice');
              res.status(200).send(result);
            })
            .catch((err) => {
              res.status(402).send(err);
            });
        })
        .catch((err) => {
          res.status(400).send(err);
        });
    })
    .catch((err) => {
      res.status(402).send(err);
    });
});
