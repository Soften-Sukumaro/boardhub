const { asyncMiddleware } = require("../middlewares/asyncMiddleware");
const {
  Product,
  Order,
  OrderProduct,
  RefOrderProductStatus,
} = require('../config/sequelize');

exports.getCart = asyncMiddleware(async (req, res, next) => {
  Order.findOne({
    where: {
      userId: req.params.id,
      refOrderStatusId: 1,
    },
  }).then((theOrder) => {
    OrderProduct.findAndCountAll({
      where: {
        orderId: theOrder.id,
      },
      include: [{
        model: Product,
        attributes: ['id', 'name', 'price', 'main_photo'],
      }, {
        model: RefOrderProductStatus,
        attributes: ['description'],
      }],
    }).then((result) => {
      //  console.log(result);
      res.status(200).send(result);
    }).catch((err) => {
      res.status(400).send(err);
    });
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getByOrderId = asyncMiddleware(async (req, res, next) => {
  // console.log('get product in an order id');
  OrderProduct.findAndCountAll({
    where: {
      orderId: req.params.id,
    },
    include: [{
      model: Product,
      attributes: ['id', 'name', 'price', 'main_photo'],
    }, {
      model: RefOrderProductStatus,
      attributes: ['description'],
    }],
  }).then((result) => {
    // console.log(result);
    if (result === null) {
      res.status(401).send({
        // auth: false,
        failed: 'order product not found',
      });
    } else {
      res.status(200).send(result);
    }
  }).catch((err) => {
    //  console.log(err);
    res.status(400).send(err);
  });
});

exports.addToCart = asyncMiddleware(async (req, res, next) => {
  //  console.log('add item to cart');
  // create if cart not exist
  Order.findOrCreate({
    where: {
      userId: req.user.id,
      refOrderStatusId: 1,
    },
    defaults: {
      userId: req.user.id,
      refOrderStatusId: 1,
    },
  }).spread((order) => {
    OrderProduct.findOrCreate({
      where: {
        orderId: order.id,
        productId: req.body.productId,
      },
      defaults: {
        amount: 1,
        productId: req.body.productId,
        orderId: order.id,
        refOrderProductStatusId: 1,
      },
    }).spread((orderProduct, justCreated) => {
      let result = 'add new item success';
      if (!justCreated) {
        orderProduct.increment('amount');
        result = 'increment success';
      }
      res.status(200).send(result);
    }).catch((err) => {
      //  console.log(err);
      res.status(400).send(err);
    });
  }).catch((err) => {
    //  console.log(err);
    res.status(400).send(err);
  });
});

exports.delete = asyncMiddleware(async (req, res, next) => {
// console.log('remove product');
  OrderProduct.destroy({
    where: {
      id: req.body.id,
    },
  }).then((rowDeleted) => {
    res.status(200).send({
      rowDeleted,
    });
  });
});

exports.clearCart = asyncMiddleware(async (req, res, next) => {
  //   console.log('clear a cart');
  OrderProduct.destroy({
    where: {
      orderId: req.body.id,
    },
  }).then((rowDeleted) => {
    res.status(200).send({
      rowDeleted,
    });
  });
});

exports.setAmount = asyncMiddleware(async (req, res, next) => {
  let count = 0;
  // console.log('set amount of orderProduct');
  for (const element of req.body) {
    if (element.amount === 0) {
      OrderProduct.destroy({
        where: {
          id: element.id,
        },
      });
    } else {
      OrderProduct.update({
        amount: element.amount,
      }, {
        where: {
          id: element.id,
        },
      }).then((result) => {
        count += parseInt(result[0]);
      });
    }
    await new Promise(resolve => setTimeout(resolve, 300));
  }
  // console.log(count);
  res.status(200).send({
    affectedRow: count,
  });
});

exports.getByRefId = asyncMiddleware(async (req, res, next) => {
  OrderProduct.findAll({
    where: {
      refOrderProductStatusId: req.params.refid,
    },
    attributes: ['id', 'amount'],
    include: [{
      model: Product,
      attributes: ['id', 'name'],
      where: {
        create_by: req.user.id,
      },
    }, {
      model: Order,
      attributes: ['id', 'userId'],
    }, {
      model: RefOrderProductStatus,
      attributes: ['description'],
     }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  })
});