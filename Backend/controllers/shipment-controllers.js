const Sequelize = require('sequelize');
const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');
const {
  Order,
  OrderProduct,
  Shipment,
  RefShipmentStatus,
} = require('../config/sequelize');

const {
  or,
} = Sequelize.Op;

exports.getAll = asyncMiddleware(async (req, res) => {
  // console.log('all shipment');
  Shipment.findAll({
    include: [{
      model: RefShipmentStatus,
      attributes: ['description'],
    }, {
      model: OrderProduct,
    }],
  }).then((result) => {
    if (result === null) {
      res.status(400).send({
        failed: 'error ocurred',
      });
    } else {
      res.status(200).send(result);
    }
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getByUser = asyncMiddleware(async (req, res) => {
  Shipment.findAll({
    where: {
      [or]: [{
        sender: req.params.id,
      },
      {
        receiver: req.params.id,
      },
      ],
    },
    include: [{
      model: RefShipmentStatus,
      attributes: ['description'],
    }, {
      model: OrderProduct,
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getSender = asyncMiddleware(async (req, res) => {
  Shipment.findAll({
    where: {
      sender: req.params.id,
    },
    include: [{
      model: RefShipmentStatus,
      attributes: ['description'],
    }, {
      model: OrderProduct,
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.getReceiver = asyncMiddleware(async (req, res) => {
  Shipment.findAll({
    where: {
      receiver: req.params.id,
    },
    include: [{
      model: RefShipmentStatus,
      attributes: ['description'],
    }, {
      model: OrderProduct,
    }],
  }).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.confirm = asyncMiddleware(async (req, res) => {
  Shipment.update({
    refShipmentStatusId: 2
  }, {
    where: {
      id: req.body.id,
    }
  }).then((rowUpdated) => {
    if (rowUpdated == 0) {
      res.status(402).send({
        failed: 'no shipment found',
      });
    } else {
      OrderProduct.update({
        refOrderProductStatusId: 5,
      }, {
        where: {
          shipmentId: req.body.id,
        },
      }).then((rowUpdated) => {
        res.status(200).send({
          rowUpdated: rowUpdated,
        });
      }).catch((err) => {
        res.status(400).send(err);
      });
    }
  }).catch((err) => {
    res.status(400).send(err);
  });
});

exports.regisItem = asyncMiddleware(async (req, res) => {
  OrderProduct.findOne({
    where: {
      id: req.body.orderProductId,
    },
    include: {
      model: Order,
      attributes: ['userId'],
    }
  }).then((orderProduct) => {
    console.log(orderProduct);
    Shipment.findOrCreate({
      where: {
        tracking_id: req.body.tracking,
      },
      defaults: {
        tracking_id: req.body.tracking,
        sender: req.user.id,
        receiver: orderProduct.order.userId,
        refShipmentStatusId: 1,
      },
    }).spread((shipment) => {
      OrderProduct.update({
        shipmentId: shipment.id,
        refOrderProductStatusId: 4,
      }, {
        where: {
          id: req.body.orderProductId,
        }
      }).then((rowUpdated) => {
        res.status(200).send({
          rowUpdated,
        });
      }).catch((err) => {
        res.status(400).send(err);
      });
    }).catch((err) => {
      res.status(400).send(err);
    });
  });
});