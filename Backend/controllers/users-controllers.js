const bcrypt = require('bcryptjs');
const AWS = require('aws-sdk');
const awsS3config = require('../config/awsS3config');
const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');
const {
  User,
} = require('../config/sequelize');
const mail = require('./mail');
const awsS3 = new AWS.S3(awsS3config);

exports.getUser = asyncMiddleware(async (req, res, next) => {
  if (!req.user) res.sendStatus(401);
  res.json({
    user: req.user,
  });
});

exports.uploadProfile = asyncMiddleware(async (req, res, next) => {
  const updateComponents = {};
  if (req.file !== undefined) {
    updateComponents.photo_link = req.file.location;
    const result = await User.findOne({
      attributes: ['photo_link'],
      where: {
        username: req.user.username,
      },
    }, {
      raw: true,
    });
    if (result.photo_link !== null) {
      const params = {
        Bucket: 'boardhubphotos',
        Key: result.photo_link.split('/').pop(),
      };
      awsS3.deleteObject(params, (err) => {
        if (err) {
          res.status(400).send(err);
        }
      });
    }
  }

  await User.update(updateComponents, {
    returning: true,
    where: {
      id: req.user.id,
    },
  }).catch(err => res.status(402).send(err));
  res.json({
    photo_link: req.file.location,
  });
});

exports.register = asyncMiddleware(async (req, res, next) => {
  const today = new Date();
  const newUser = {
    first_name: req.body.firstname,
    last_name: req.body.lastname,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
    city: req.body.city,
    country: req.body.country,
    phone_number: req.body.phone,
    address: req.body.address,
    gender: req.body.gender,
    created: today,
    role: 'user',
  };

  const salt = await bcrypt.genSalt(10);
  newUser.password = await bcrypt.hash(newUser.password, salt);
  await User.create(newUser).catch(err => res.status(400).send(err));
  res.status(201).send({
    auth: true,
    user: newUser.username,
    success: 'user registered sucessfully',
    role: 'user',
  });
  await mail.sendMailText(req.body.email, 'Boardhub User Register Seccess', 'Success');
});

exports.updateUser = asyncMiddleware(async (req, res, next) => {
  const updateComponents = req.body;

  // Fix some Variable to not change
  updateComponents.username = req.user.username;
  updateComponents.role = req.user.role;
  updateComponents.id = req.user.id;
  updateComponents.created = req.user.created;
  updateComponents.password = req.user.password;
  updateComponents.email = req.user.email;

  // console.log(updateComponents);
  User.update(updateComponents, {
      returning: true,
      where: {
        id: req.user.id,
      },
    })
    .then((rowUpdated) => {
      res.json(rowUpdated);
    })
    .catch(err => res.status(402).send(err));
});

exports.login = asyncMiddleware(async (req, res, next) => {
  if (req.user) {
    const json = {
      id: req.user.id,
      username: req.user.username,
      role: req.user.role,
    };
    res.status(200).json(json);
  } else res.sendStatus(401);
});

exports.getUserDetail = asyncMiddleware(async (request, response, next) => {
  // console.log('get detail');
  if (request.params.username === 'me' || request.user.username === request.params.username ) {
    // console.log(request.user);

    const result = await User.findOne({
      where: {
        username: request.user.username,
      },
    }).catch(err => response.status(400).send(err));
    if (result === null) {
      response.status(401).send({
        failed: 'user not found',
      });
    } else {
      response.status(200).send(result);
    }
  }
  else if( request.user.role =='admin'|| request.user.role =='creator' ) { 
    const result = await User.findOne({
      where: {
        username: request.params.username,
      },
    }).catch(err => response.status(400).send(err));
    if (result === null) {
      response.status(401).send({
        failed: 'user not found',
      });
    } else {
      response.status(200).send(result);
    }
  } 
  else {
    const result = await User.findOne({
      attributes: [
        'first_name',
        'last_name',
        'email',
        'username',
        'country',
        'city',
      ],
      where: {
        username: request.params.username,
      },
    }).catch(err => response.status(400).send(err));
    if (result === null) {
      response.status(401).send({
        failed: 'user not found',
      });
    } else {
      response.status(200).send(result);
    }
  }
});

exports.getUserDetailwoauthor = asyncMiddleware(async (request, response, next) => {
  const result = await User.findOne({
    attributes: ['first_name', 'last_name', 'email', 'username', 'country', 'city', 'photo_link'],
    where: {
      username: request.params.id,
    },
  }).catch(err => response.status(500).send(err));
  if (result === null) {
    return response.status(400).send({
      message: 'Cannot find this username',
    });
  }
  return response.status(200).send(result);
});

exports.checkSameUsername = asyncMiddleware(async (request, response, next) => {
  const count = await User.count({
    where: {
      username: request.params.user,
    },
  });
  let found = false;
  if (count > 0) {
    found = true;
  }
  response.send({
    result: (found ? 'Same' : 'Available'),
    searchfor: 'username',
  });
});

exports.checkSameEmail = asyncMiddleware(async (request, response, next) => {
  const count = await User.count({
    where: {
      email: request.params.email,
    },
  });
  let found = false;
  if (count > 0) {
    found = true;
  }
  response.send({
    result: (found ? 'Same' : 'Available'),
    searchfor: 'email',
  });
});

exports.getAllUsers = asyncMiddleware(async (request, response, next) => {
  /* if (request.user.role !== 'admin' || request.user.role !== 'creator') {
    response.status(403).send({
      message: 'NEED Admin privilleged',
    });
  } */
  const result = await User.findAll({
    attributes: ['id', 'username', 'role'],
  }).catch(err => response.status(400).send(err));
  if (result === null) {
    response.status(400).send({
      failed: 'error ocurred',
    });
  } else {
    response.status(200).send(result);
  }
});

exports.logout = asyncMiddleware(async (req, res, next) => {
  req.logout();
  res.sendStatus(200);
});

exports.deleteUser = asyncMiddleware(async (req, res, next) => {
  // const result = await User.findOne({
  //   attributes: ['photo_link'],
  //   where: {
  //     username: req.params.username,
  //   },
  // }, {
  //   raw: true,
  // }).catch(err => res.status(400).send(err));

  // if (result.photo_link) {
  //   const params = {
  //     Bucket: 'boardhubphotos',
  //     Key: result.photo_link.split('/').pop(),
  //   };
  //   awsS3.deleteObject(params, (err) => {
  //     if (err) {
  //       res.status(400).send(err);
  //     }
  //   });
  // }

  await User.destroy({
    where: {
      username: req.params.username,
    },
  });

  res.sendStatus(200);
});