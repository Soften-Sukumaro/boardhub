module.exports = function f(req, res, next) {
  if (req.isAuthenticated() && req.user.role === 'admin') {
    return next();
  }
  res.sendStatus(401);
  return 0;
};
