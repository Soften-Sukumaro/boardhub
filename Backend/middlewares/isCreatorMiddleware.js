module.exports = function f(req, res, next) {
  if (req.isAuthenticated() && (req.user.role === 'admin' || req.user.role === 'creator')) {
    return next();
  }
  res.sendStatus(401);
  return 0;
};
