module.exports = (sequelize, type) => sequelize.define('confirmation', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: {
      arg: false,
      msg: 'Primary Key must not NULL',
    },
  },
  message: {
    type: type.TEXT,
  },
  transfer_datetime: {
    type: type.DATE,
  },
  create_by: {
    type: type.INTEGER,
  },
  photo_link: {
    type: type.STRING,
  },
});
