module.exports = (sequelize, type) => sequelize.define('invoice', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: {
      arg: false,
      msg: 'Primary Key must not NULL',
    },
  },
  price: {
    type: type.INTEGER,
    defaultValue: 1,
    allowNull: {
      arg: false,
      msg: 'Price must not NULL',
    },
  },
  detail: {
    type: type.TEXT,
  },
});
