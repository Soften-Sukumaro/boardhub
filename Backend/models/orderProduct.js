module.exports = (sequelize, type) => sequelize.define('orderProduct', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: {
      arg: false,
      msg: 'Primary Key must not NULL',
    },
  },
  amount: {
    type: type.INTEGER,
    defaultValue: 1,
    allowNul: {
      arg: false,
      msg: 'Amount must not NULL',
    },
  },
});
