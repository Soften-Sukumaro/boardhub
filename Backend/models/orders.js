module.exports = (sequelize, type) => sequelize.define('orders', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: {
      arg: false,
      msg: 'Primary Key must not NULL',
    },
  },
  date_order_place: {
    type: type.DATE,
  },
});
