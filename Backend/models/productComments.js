module.exports = (sequelize, type) => sequelize.define('productComment', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: {
      args: false,
      msg: 'Primary Key must not NULL',
    },
  },
  comment: {
    type: type.STRING,
  },
  productId: {
    type: type.STRING,
  },
  userId: {
    type: type.STRING,
  },
});
