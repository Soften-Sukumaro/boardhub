module.exports = (sequelize, type) => sequelize.define('productPhoto', {
  photo_link: {
    type: type.STRING,
    primaryKey: true,
    allowNull: {
      args: false,
      msg: 'Primary Key must not NULL',
    },
  },
});
