module.exports = (sequelize, type) => sequelize.define('product', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: {
      args: false,
      msg: 'Primary Key must not NULL',
    },
  },
  return_merchandise_authorization: {
    type: type.STRING,
  },
  name: {
    type: type.STRING,
    allowNull: {
      args: false,
      msg: 'Product Name must not NULL',
    },
  },
  type: {
    type: type.STRING,
  },
  min_player: {
    type: type.INTEGER,
  },
  max_player: {
    type: type.INTEGER,
  },
  duration: {
    type: type.STRING,
  },
  description: {
    type: type.TEXT,
  },
  price: {
    type: type.STRING,
  },
  quantity: {
    type: type.INTEGER,
  },
  main_photo: {
    type: type.STRING,
  },
  last_update_by: {
    type: type.INTEGER,
  },
  create_by: {
    type: type.INTEGER,
  },
});