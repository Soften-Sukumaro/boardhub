module.exports = (sequelize, type) => sequelize.define('refInvoiceStatus', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    allowNull: {
      arg: false,
      msg: 'Primary Key must not NULL',
    },
  },
  description: {
    type: type.TEXT,
    allowNull: {
      arg: false,
      msg: 'Description must not NULL',
    },
  },
});
