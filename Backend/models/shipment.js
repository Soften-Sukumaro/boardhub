module.exports = (sequelize, type) => sequelize.define('shipments', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: {
      arg: false,
      msg: 'Primary Key must not NULL',
    },
  },
  tracking_id: {
    type: type.STRING,
    allowMull: {
      arg: false,
      msg: 'tracking_id must not NULL',
    },
  },
});
