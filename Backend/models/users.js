module.exports = (sequelize, type) => sequelize.define('user', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  gender: {
    type: type.STRING,
  },
  first_name: {
    type: type.STRING,
  },
  last_name: {
    type: type.STRING,
  },
  email: {
    type: type.STRING,
    unique: {
      args: true,
      msg: 'Email address already in use!',
    },
  },
  username: {
    type: type.STRING,
    unique: {
      args: true,
      msg: 'Username already in use!',
    },
    allowNull: {
      args: false,
      msg: 'Username must not NULL',
    },
  },
  password: {
    type: type.STRING,
    allowNull: {
      args: false,
      msg: 'Password must not NULL',
    },
  },
  phone_number: {
    type: type.STRING,
  },
  address: {
    type: type.STRING,
  },
  city: {
    type: type.STRING,
  },
  country: {
    type: type.STRING,
  },
  role: {
    type: type.STRING,
    allowNull: {
      args: false,
      msg: 'Role must not NULL',
    },
  },
  photo_link: {
    type: type.STRING,
  },
});
