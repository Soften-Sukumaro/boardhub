# Backend

# Routes
## Users
### Post Register
>/api/users/register
>
Require
> body -> firstname\
> body -> lastname\
> body -> email\
> body -> username\
> body -> password\
> body -> city\
> body -> country\
> body -> phone\
> body -> address\
> body -> gender

Return 
>auth (true/false)\
> token (JWT Token)\
> user (Username)\
> success (Status)\
> role (admin/user)

### Post Login
>/api/users/login

Require
>body ->username\
>body ->password
>
Return 
>auth (true/false)\
>token (JWT Token)\
>user (Username)\
>success (Status)\
>role (admin/user)

### Get users detail
>/api/users/get/detail/:username(or me)
>
Require
>header -> Authorization (Bearer >Your_JWT_Token)
>
Return 
> detail of username in JWT_Token
>

### Get users detail without authorization
> /get/detailwoauthor/:username

Return
> first_name, last_name, email, username, country, city,photo_link of this username

### Get Check Same username
>/api/users/get/username?search=YOUR_USERNAME
>
Return 
>true/false
### Get Check Same email
>/api/users/get/email?search=YOUR_EMAIL
>
Return 
>true/false
### Get all users
>/api/users/all
>
Require
> header -> Authorization (Bearer >Your_JWT_Token)(ADMIN ONLY)

Return
> all users info as JSON 
## Products
### Get all products
>/api/products/all
>
> 
Return 
> list of all products and their details

### Post add products
>/api/products/add
>
Require
> body -> product_type\
> body -> return_authorization\
> body -> name\
> body -> price\
> body -> color\
> body -> size\
> body -> description\
> body -> other_product_details

Return 
>product: product_name
## Admins
### Post Register Admin
> /api/admins/register

Require
>  body -> firstname  \
>body -> lastname  \
>body -> email  \
>body -> username  \
>body -> password \
>body -> city  \
>body -> country  \
>body -> phone  \
>body -> address\
>body -> gender
>
Return
>auth (true/false)  \
>token (JWT Token)  \
>user (Username)  \
>success (Status)  \
>role (admin/user)

## Order
### Get all order (require admin)
> /api/orders/all

Return
>All Order Model

### Get order by user's id
> /api/orders/get/byuser/THE_USER_ID

Return
>Order Model

### Get order detail
> /api/orders/get/detail/THE_ORDER_ID

Return
>Only 1 Order Model

## OrderProduct
### Get product in order
>/api/orderProducts/byorder/THE_ORDER_ID

Return
> OrderProduct Model

### Add to cart
>/api/orderProducts/addtocart

Require
> body -> productId

Return
> text (add new item success / increment success)

### Remove product out of order
>/api/orderProducts/remove

Require
> body -> orderProductId

Return
> "%d row deleted", number of product that was delete(should be only 1)

### Clear cart
>/api/orderProducts/clearcart

Require
> body -> orderId

Return
> "%d row deleted", number of product that was delete


## Token
Can be decode at https://jwt.io/
Data in Token
> "id": username,\
>  "role": (admin/user)
