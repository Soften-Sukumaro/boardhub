const express = require('express');
const passport = require('passport');
const isAdminChecker = require('../middlewares/isAdminMiddleware');
const adminsCTRL = require('../controllers/admins-controllers');

const router = express.Router();

// Post Register Admin SEQ JEST
router.post('/register', adminsCTRL.register);

// Post Login SEQ JEST
router.post('/login', passport.authenticate('local'), adminsCTRL.login);

// Post log out JEST
router.post('/logout', adminsCTRL.logout);

// Post upgrade user to creator
router.post('/role', isAdminChecker, adminsCTRL.changerole);

module.exports = router;
