const express = require('express');
const orders = require('./orders');
const orderProducts = require('./orderProduct');
const confirmations = require('./confirmation');
const invoice = require('./invoice');
const users = require('./users');
const products = require('./products');
const admins = require('./admins');
const comments = require('./comments');
const shipments = require('./shipment');

const router = express.Router();

// Path /api/comments
router.use('/comments', comments);

// Path /api/orders
router.use('/orders', orders);

// Path /api/orderProducts
router.use('/orderProducts', orderProducts);

// Path /api/confirmations
router.use('/confirmations', confirmations);

// Path /api/invoices
router.use('/invoices', invoice);

// Path /api/users
router.use('/users', users);

// Path /api/admins
router.use('/admins', admins);

// Path /api/shipments
router.use('/shipments', shipments);

// Path /api/products
router.use('/products', products);

router.get('/test', (req, res) => res.send('Welcome to boardhub backend FULL CICD'));

// Path /api/
router.use('/', (req, res) => res.sendStatus(404));

// Other /api Path
router.use('*', (req, res) => res.sendStatus(404));

module.exports = router;

