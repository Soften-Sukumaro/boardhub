const express = require('express');
const isAuthenticated = require('../middlewares/isAuthenticateMiddleware');
const commentsCTRL = require('../controllers/comments-controllers');

const router = express.Router();

// Put(change) Comment JEST
router.put('/', isAuthenticated, commentsCTRL.changeComments);

// Post Product Comment JEST
router.post('/', isAuthenticated, commentsCTRL.addComments);

// Delete Comments JEST
router.delete('/:id', isAuthenticated, commentsCTRL.deleteComments);

// Get Product Comment JEST
router.get('/:id', commentsCTRL.getComments);

module.exports = router;
