const express = require('express');
const isAuthenticated = require('../middlewares/isAuthenticateMiddleware');
const multerConfig = require('../config/multer');
const isAdmin = require('../middlewares/isAdminMiddleware');
const confirmationCTRL = require('../controllers/confirmation-controllers');

const router = express.Router();
const uploadS3 = multerConfig.uploadS3confirmation;

router.post('/add', isAuthenticated, confirmationCTRL.add);

router.post('/addphoto', isAuthenticated, uploadS3.single('photo'), confirmationCTRL.addphoto);

router.post('/confirm', isAdmin, confirmationCTRL.confirm);

router.get('/get/unconfirmed/byuser/:id', isAuthenticated, confirmationCTRL.getUnconfirmByUser);

router.get('/get/confirmed/byuser/:id', isAuthenticated, confirmationCTRL.getConfirmedByUser);

router.get('/get/detail/:id', isAuthenticated, confirmationCTRL.getDetail);

router.get('/get/byorder/:id', isAuthenticated, confirmationCTRL.getByOrder);

router.get('/get/picture/:id', isAuthenticated, confirmationCTRL.getPicture);

router.get('/get/all', isAdmin, confirmationCTRL.getAll);

module.exports = router;