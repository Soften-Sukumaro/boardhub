const express = require('express');
const isAuthenticated = require('../middlewares/isAuthenticateMiddleware');
const invoiceCTRL = require ('../controllers/invoice-controllers');

const router = express.Router();

router.post('/add', isAuthenticated, invoiceCTRL.add);

router.get('/get/detail/:id', isAuthenticated, invoiceCTRL.getDetail);

router.get('/get/unpaid/byuser/:id' , isAuthenticated, invoiceCTRL.getUnpaidByUser);

router.get('/get/byorder/:id', isAuthenticated, invoiceCTRL.getByOrder);

module.exports = router;
