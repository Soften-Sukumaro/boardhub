const express = require('express');
const isAuthenticated = require('../middlewares/isAuthenticateMiddleware');
const isCreator = require('../middlewares/isCreatorMiddleware');
const orderProductCTRL = require('../controllers/orderProduct-controllers')

const router = express.Router();

router.get('/get/cart/byuser/:id', isAuthenticated, orderProductCTRL.getCart);

// get item in order
router.get('/get/byorder/:id', isAuthenticated, orderProductCTRL.getByOrderId);

// Add item to cart
router.post('/addtocart', isAuthenticated,orderProductCTRL.addToCart);


// remove product out of order
router.post('/delete', isAuthenticated, orderProductCTRL.delete);

// clear cart
router.post('/clearcart', isAuthenticated, orderProductCTRL.clearCart);

// set orderProduct amount
router.post('/setamount', isAuthenticated, orderProductCTRL.setAmount);

router.get('/get/byref/:refid', isCreator, orderProductCTRL.getByRefId);

module.exports = router;
