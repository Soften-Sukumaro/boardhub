const express = require('express');
const Sequelize = require('sequelize');
const isAuthenticated = require('../middlewares/isAuthenticateMiddleware');
const isAdmin = require('../middlewares/isAdminMiddleware');
const ordersCTRL = require('../controllers/order-controllers')

const router = express.Router();

// get all order
router.get('/all', isAdmin, ordersCTRL.getAll);

// get order by userid
router.get('/get/byuser/:id', isAuthenticated, ordersCTRL.getByUserId);

router.get('/get/cart/byuser/:id', isAuthenticated, ordersCTRL.getCart);

router.get('/get/noncart/byuser/:id', isAuthenticated, ordersCTRL.getNonCart);

// get an order detail
router.get('/get/detail/:id', isAuthenticated, ordersCTRL.getDetail);


router.post('/checkout', isAuthenticated, ordersCTRL.checkout);

module.exports = router;
