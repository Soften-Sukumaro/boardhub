const express = require('express');
const AWS = require('aws-sdk');
const Sequelize = require('sequelize');
const isAuthenticated = require('../middlewares/isAuthenticateMiddleware');
const isAdmin = require('../middlewares/isAdminMiddleware');
const isCreator = require('../middlewares/isCreatorMiddleware');
const awsS3config = require('../config/awsS3config');
const multerConfig = require('../config/multer');
const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');
const {
  Product,
  ProductPhoto,
  ProductComment,
} = require('../config/sequelize');

const Op = Sequelize.Op;

const router = express.Router();
const awsS3 = new AWS.S3(awsS3config);
const uploadS3 = multerConfig.uploadS3product;

const productPhotoss3 = uploadS3.fields([{
  name: 'main_photo',
  maxCount: 1,
}, {
  name: 'photos',
  maxCount: 8,
}]);

// Upload Photo
router.post('/photo', isAuthenticated, productPhotoss3, asyncMiddleware(async (request, response) => {
  if (request.body.photos) {
    if (request.body.photos.toString().split(',').length > 1) {
      const photos = [];
      request.files.photos.map((file) => {
        photos.push(file.location);
      });
    } else {
      response.status(200).send({
        main_photo: request.files.main_photo[0].location,
        photos: request.files.photos[0].location,
      });
    }
  } else {
    response.status(200).send({
      main_photo: request.files.main_photo[0].location,
    });
  }
}));

// Put photos s3
// router.put('/photos/:id', isAdmin, productPhotoss3, asyncMiddleware(async (req, res) => {
//   const json = {
//     main_photo: '',
//     photos: [],
//   };
//   if (req.files !== undefined) {
//     if (req.files.main_photo !== undefined) {
//       const updateComponents = {};
//       updateComponents.main_photo = req.files.main_photo[0].location;
//       updateComponents.last_update_by = req.user.id;
//       json.main_photo = req.files.main_photo[0].location;

//       const result = await Product.findOne({
//         attributes: ['main_photo'],
//         where: {
//           id: req.params.id,
//         },
//       }, {
//         raw: true,
//       });
//       if (result.main_photo !== null) {
//         const params = {
//           Bucket: 'boardhubphotos',
//           Key: result.main_photo.split('/').pop(),
//         };
//         awsS3.deleteObject(params, (err) => {
//           if (err) {
//             res.status(400).send(err);
//           }
//         });
//       }
//       Product.update(updateComponents, {
//         returning: true,
//         where: {
//           id: req.params.id,
//         },
//       }, {
//         include: [ProductPhoto],
//       }).catch(err => res.status(402).send(err));
//     }
//     if (req.files.photos !== undefined) {
//       const updateComponents = {};
//       updateComponents.last_update_by = req.user.id;
//       await Product.update(updateComponents, {
//         where: {
//           id: req.params.id,
//         },
//       });
//       json.photos = [];
//       req.files.photos.map((file) => {
//         json.photos.push(file.location);
//       });

//       const result = await ProductPhoto.findAll({
//         attributes: ['photo_link'],
//         where: {
//           productId: req.params.id,
//         },
//       }, {
//         raw: true,
//       });

//       result.map((f) => {
//         if (f.photo_link !== null) {
//           const params = {
//             Bucket: 'boardhubphotos',
//             Key: f.photo_link.split('/').pop(),
//           };
//           awsS3.deleteObject(params, (err) => {
//             if (err) {
//               res.status(400).send(err);
//             }
//           });
//         }
//       });

//       ProductPhoto.destroy({
//         where: {
//           productId: req.params.id,
//         },
//       });

//       req.files.photos.map((file) => {
//         ProductPhoto.create({
//           photo_link: file.location,
//           productId: req.params.id,
//         });
//       });
//     }

//     res.json(json);
//   }
// }));

// Get my product
router.get('/me', isAuthenticated, asyncMiddleware(async (request, response) => {
  const product = await Product.findAll({
    where: {
      create_by: request.user.id,
    },
    include: [{
      model: ProductPhoto,
    }, {
      model: ProductComment,
    }],
  }).catch(err => response.status(400).send(err));
  response.send(product);
}));

// Get all products
router.get('/all', (request, response) => {
  console.log('\n\n\nALL\n\n\n');
  Product.findAll({
    include: [{
      model: ProductPhoto,
    }, {
      model: ProductComment,
    }],
  }).then((result) => {
    response.status(200).send(result);
  }).catch(err => response.status(402).send({
    error: err.errors[0].message,
  }));
});

// Get one product
router.get('/:id', (request, response) => {
  Product.findOne({
    where: {
      id: request.params.id,
    },
    include: [{
      model: ProductPhoto,
    }, {
      model: ProductComment,
    }],
  }).then(result => response.status(200).send(result)).catch(err => response.status(402).send(err));
});

// Post add products
router.post('/add', isCreator, asyncMiddleware(async (request, response) => {
  const newProduct = request.body;
  newProduct.create_by = request.user.id;
  newProduct.main_photo = request.body.main_photo;
  const createReturn = await Product.create(newProduct).catch(err => response.status(402).send(err));

  if (request.body.photos) {
    // console.log(request.body.photos);
    if (request.body.photos.toString().split(',').length > 1) {
      await request.body.photos.map(asyncMiddleware(async (p) => {
        ProductPhoto.create({
          photo_link: p,
          productId: createReturn.id,
        }).catch(err => response.status(402).send(err));
      }));
      response.send({
        id: createReturn.id,
        product: createReturn.name,
        success: 'product add sucessfully',
      });
    } else {
      ProductPhoto.create({

        photo_link: request.body.photos,
        productId: createReturn.id,
      }).catch(err => response.status(402).send(err)).then(() => response.send({
        id: createReturn.id,
        product: createReturn.name,
        success: 'product add sucessfully',
      }));
    }
  } else {
    response.send({
      id: createReturn.id,
      product: createReturn.name,
      success: 'product add sucessfully',
    });
  }
}));

// Update Products
router.put('/:id', isCreator, asyncMiddleware(async (req, res) => {
  const updateComponents = req.body;
  updateComponents.last_update_by = req.user.id;
  await Product.update(updateComponents, {
    returning: true,
    where: {
      id: req.params.id,
    },
  }, {
    include: [ProductPhoto],
  }).catch(err => res.status(402).send(err));

  res.json({
    id: req.params.id,
    updated: updateComponents,
  });
}));

// Delete product
router.delete('/:id', isCreator, asyncMiddleware(async (request, response) => {
  const result = await Product.findOne({
    attributes: ['main_photo'],
    where: {
      id: request.params.id,
    },
  }, {
    raw: true,
  });
  if (result.main_photo !== null) {
    const params = {
      Bucket: 'boardhubphotos',
      Key: result.main_photo.split('/').pop(),
    };
    awsS3.deleteObject(params, (err) => {
      if (err) {
        response.status(400).send(err);
      }
    });
  }

  const result2 = await ProductPhoto.findAll({
    attributes: ['photo_link'],
    where: {
      productId: request.params.id,
    },
  }, {
    raw: true,
  });
  result2.map((f) => {
    if (f.photo_link !== null) {
      const params = {
        Bucket: 'boardhubphotos',
        Key: f.photo_link.split('/').pop(),
      };
      awsS3.deleteObject(params, (err) => {
        if (err) {
          response.status(400).send(err);
        }
      });
    }
  });

  await Product.destroy({
    where: {
      id: request.params.id,
    },
  });

  await ProductPhoto.destroy({
    where: {
      productId: request.params.id,
    },
  });

  await ProductPhoto.destroy({
    where: {
      productId: null,
    },
  });

  response.sendStatus(200);
}));

// Post Search Product by name and types
router.post('/search', (req, res) => {
  let text = `%${req.body.search}%`;
  text = text.split(' ').join('%');
  if (!req.body.type) {
    Product.findAll({
      where: {
        name: {
          [Op.like]: text,
        },
      },
      include: [{
        model: ProductPhoto,
      }, {
        model: ProductComment,
      }],
    }).then((result) => {
      res.send(result);
    }).catch(err => res.status(400).send(err));
  } else if (req.body.type.toString().split(',').length > 1) {
    Product.findAll({
      where: {
        name: {
          [Op.like]: text,
        },
        type: {
          [Op.or]: [req.body.type],
        },
      },
      include: [{
        model: ProductPhoto,
      }, {
        model: ProductComment,
      }],
    }).then((result) => {
      res.send(result);
    }).catch(err => res.status(400).send(err));
  } else {
    Product.findAll({
      where: {
        name: {
          [Op.like]: text,
        },
        type: req.body.type,
      },

      include: [{
        model: ProductPhoto,
      }, {
        model: ProductComment,
      }],
    }).then((result) => {
      res.send(result);
    }).catch(err => res.status(400).send(err));
  }
});

// Get all product Type
router.get('/type/all', (req, res) => {
  Product.findAll({
    attributes: ['type', [Sequelize.fn('COUNT', Sequelize.col('type')), 'count']],
    where: {
      type: {
        [Op.ne]: null,
      },
    },
    group: 'type',
  }).then(result => res.send(result)).catch(err => res.status(400).send(err));
});

module.exports = router;
