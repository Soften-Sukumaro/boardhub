const express = require('express');
const Sequelize = require('sequelize');
const isAuthenticated = require('../middlewares/isAuthenticateMiddleware');
const isAdmin = require('../middlewares/isAdminMiddleware');
const isCreator = require('../middlewares/isCreatorMiddleware');
const shipmentCTRL = require('../controllers/shipment-controllers');

const router = express.Router();

router.get('/all', isAdmin, shipmentCTRL.getAll);

router.get('/get/all/byuser/:id', isAuthenticated, shipmentCTRL.getByUser);

router.get('/get/sender/byuser/:id', isCreator, shipmentCTRL.getSender);

router.get('/get/receiver/byuser/:id', isAuthenticated, shipmentCTRL.getReceiver);

router.post('/confirm', isAuthenticated, shipmentCTRL.confirm);

router.post('/regisitem', isCreator, shipmentCTRL.regisItem);

module.exports = router;
