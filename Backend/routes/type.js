const express = require('express');
const {
  RefProductType,
} = require('../config/sequelize');

const router = express.Router();

// Post add products type SEQ
router.get('/add/:type', (request, response) => {
  RefProductType.create({
    description: request.params.type,
  }).then(() => response.send(200))
    .catch(err => response.status(402).send({
      error: err.errors[0].message,
    }));
});

module.exports = router;
