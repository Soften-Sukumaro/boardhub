const express = require('express');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const AWS = require('aws-sdk');
const isAuthenticated = require('../middlewares/isAuthenticateMiddleware');
const isAdmin = require('../middlewares/isAdminMiddleware');
const multerConfig = require('../config/multer');
const awsS3config = require('../config/awsS3config');
const {
  asyncMiddleware,
} = require('../middlewares/asyncMiddleware');
const {
  User,
} = require('../config/sequelize');

const router = express.Router();
const awsS3 = new AWS.S3(awsS3config);
const uploadS3 = multerConfig.uploadS3user;
const usersCTRL = require('../controllers/users-controllers');
// Get user JEST
router.get('/user', isAuthenticated, usersCTRL.getUser);

// Put Profile Photo upload s3
router.put('/profile', isAuthenticated, uploadS3.single('profile'), usersCTRL.uploadProfile);

// Post Register SEQ JEST
router.post('/register', usersCTRL.register);

// Update Users JEST
router.put('/edit', isAuthenticated, usersCTRL.updateUser);

// Post Login SEQ JEST
router.post('/login', passport.authenticate('local'), usersCTRL.login);

// Get users detail SEQ JEST
router.get('/get/detail/:username', isAuthenticated, usersCTRL.getUserDetail);

// Get users detail without authorization SEQ
router.get('/get/detailwoauthor/:id', usersCTRL.getUserDetailwoauthor);

// Check Same username SEQ JEST
router.get('/search/username/:user', usersCTRL.checkSameUsername);

// Check Same email SEQ JEST
router.get('/search/email/:email', usersCTRL.checkSameEmail);

// Get all users SEQ JEST
router.get('/all', isAuthenticated, usersCTRL.getAllUsers);

// Post log out JEST
router.post('/logout', usersCTRL.logout);

// Delete user JEST
router.delete('/:username', isAdmin, usersCTRL.deleteUser);

module.exports = router;
