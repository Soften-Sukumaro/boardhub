const axios = require('axios');
// const jest = require('jest');
const link = 'http://localhost:3000/api/admins';
const login = require('./login');
const logout = require('./logout');
// FINISH
describe('Test admins path', () => {
  const uuid = Math.random() * 10;
  const mockUser = {
    email: `jesttest${uuid}@jest.test`,
    username: `Jest${uuid}`,
    password: `Jest${uuid}`,
  };

  it('should register admins without any error', async () => {
    const response = await axios.post(`${link}/register`, mockUser);
    expect(response.status).toBe(201);
    expect(response.data.user).toBe(mockUser.username);
    expect(response.data.role).toBe('admin');
    await logout();
  });

  it('admin should be able to log in', async () => {
    const response = await login(mockUser.username, mockUser.password);
    // id = res;
    expect(response.data.role).toBe('admin');
    expect(response.data.username).toBe(mockUser.username);
    expect(response.status).toBe(200);
    await logout();
  });

  it('admin can upgrade from user to creator', async () => {
    await login('jest', 'jest');
    const res = await axios.post(`${link}/role`, {
      username: 'iamuser',
      role: 'creator',
    }, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    const res2 = await login('iamuser', 'iamuser');
    expect(res2.status).toBe(200);
    expect(res2.data.role).toBe('creator');
    expect(res2.data.username).toBe('iamuser');
    await logout();
  });

  it('admin can upgrade from creator to admin', async () => {
    await login('jest', 'jest');
    const res = await axios.post(`${link}/role`, {
      username: 'iamuser',
      role: 'admin',
    }, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    const res2 = await login('iamuser', 'iamuser');
    expect(res2.status).toBe(200);
    expect(res2.data.role).toBe('admin');
    expect(res2.data.username).toBe('iamuser');
    await logout();
  });

  it('admin can downgrade from admin to creator', async () => {
    await login('jest', 'jest');
    const res = await axios.post(`${link}/role`, {
      username: 'iamuser',
      role: 'creator',
    }, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    const res2 = await login('iamuser', 'iamuser');
    expect(res2.status).toBe(200);
    expect(res2.data.role).toBe('creator');
    expect(res2.data.username).toBe('iamuser');
    await logout();
  });

  it('admin can downgrade from creator to user', async () => {
    await login('jest', 'jest');
    const res = await axios.post(`${link}/role`, {
      username: 'iamuser',
      role: 'user',
    }, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    const res2 = await login('iamuser', 'iamuser');
    expect(res2.status).toBe(200);
    expect(res2.data.role).toBe('user');
    expect(res2.data.username).toBe('iamuser');
    await logout();
  });

  it('admin can upgrade from user to admin', async () => {
    await login('jest', 'jest');
    const res = await axios.post(`${link}/role`, {
      username: 'iamuser',
      role: 'admin',
    }, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    const res2 = await login('iamuser', 'iamuser');
    expect(res2.status).toBe(200);
    expect(res2.data.role).toBe('admin');
    expect(res2.data.username).toBe('iamuser');
    await logout();
  });

  it('admin can downgrade from admin to user', async () => {
    await login('jest', 'jest');
    const res = await axios.post(`${link}/role`, {
      username: 'iamuser',
      role: 'user',
    }, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    const res2 = await login('iamuser', 'iamuser');
    expect(res2.status).toBe(200);
    expect(res2.data.role).toBe('user');
    expect(res2.data.username).toBe('iamuser');
    await logout();
  });

  it('admin should be able to log out', async () => {
    const response = await logout();
    expect(response.status).toBe(200);
  });

  it('admin can delete admin', async () => {
    await login('jest', 'jest');
    const res2 = await axios.delete(`http://localhost:3000/api/users/${mockUser.username}`, {
      withCredentials: true,
    });
    expect(res2.status).toBe(200);
    await logout();
  });
});
