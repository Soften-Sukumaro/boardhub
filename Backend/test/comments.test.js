const axios = require('axios');
// const jest = require('jest');
const link = 'http://localhost:3000/api/comments';
const login = require('./login');
const logout = require('./logout');
//FINISH
describe('Test Comments path', () => {
  let commentId;
  it('user can comment on any product', async () => {
    await login('jest', 'jest');
    const res = await axios.post(`${link}`, {
      id: 73,
      comment: 'JEST',
    }, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    commentId = res.data.id;
    const g = await get(commentId);
    expect(g.data.comment).toBe('JEST');
    await logout();
  });
  it('user can change their comment', async () => {
    await login('jest', 'jest');
    const res = await axios.put(link, {
      id: commentId,
      comment: 'JESTUPDATE',
    }, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    const g = await get(commentId);
    expect(g.data.comment).toBe('JESTUPDATE');
  });

  it('user can delete their comment', async () => {
    await login('jest', 'jest');
    const res = await axios.delete(`${link}/${commentId}`, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
  });
});

async function get(id) {
  const res = await axios.get(`${link}/${id}`);
  return res;
}