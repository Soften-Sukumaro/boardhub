const axios = require('axios');

module.exports = async function login(username, password) {
  const res = await axios.post('http://localhost:3000/api/users/login', {
    username,
    password,
  }, {
    withCredentials: true,
  });
  return res;
};
