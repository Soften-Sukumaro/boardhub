const axios = require('axios');

module.exports = async function logout() {
  const res = await axios.post('http://localhost:3000/api/users/logout', {
    withCredentials: true,
  });
  return res;
};
