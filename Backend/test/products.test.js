const axios = require('axios');
// const jest = require('jest');
const link = 'http://localhost:3000/api/products';
const login = require('./login');
const logout = require('./logout');
//

describe('Test Products path', () => {
  const uuid = Math.random() * 10;
  const mockCreatorProduct = {
    type: `creator test${uuid}`,
    name: `creator test${uuid}`,
    price: `${uuid}`,
    min_player: `${uuid}`,
    max_player: `${uuid * 100}`,
    duration: `${uuid}`,
    quantity: `${uuid}`,
    description: `${uuid}`,
  };
  const mockAdminProduct = {
    type: `admin test${uuid}`,
    name: `admin test${uuid}`,
    price: `${uuid}`,
    min_player: `${uuid}`,
    max_player: `${uuid * 100}`,
    duration: `${uuid}`,
    quantity: `${uuid}`,
    description: `${uuid}`,
  };
  it('creator can add product', async () => {
    await login('jestcreator', 'jestcreator');
    // const res = await axios.post(`${link}/add`, mockCreatorProduct, {
    //   withCredentials: true,
    // });
  });
  it('creator can update product', async () => {

  });
  it('creator can delete product', async () => {

  });
  it('admin can add product',
    async () => { await login('jestadmin', 'jestadmin'); });
  it('admin can update product', async () => {

  });
  it('admin can delete product', async () => {

  });
  it('user can get all products',
    async () => { await login('jestuser', 'jestuser'); });
  it('user can get his products', async () => {

  });
  it('user can one his products', async () => {

  });
});
