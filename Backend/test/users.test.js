const axios = require('axios');
// const jest = require('jest');
const link = 'http://localhost:3000/api/users';
const login = require('./login');
const logout = require('./logout');
//FINISH
describe('Test users path', () => {
  const uuid = Math.random() * 10;
  const mockUser = {
    email: `jesttest${uuid}@jest.test`,
    username: `Jest${uuid}`,
    password: `Jest${uuid}`,
  };

  it('should register users without any error', async () => {
    const response = await axios.post(`${link}/register`, mockUser, {
      withCredentials: true,
    });
    expect(response.status).toBe(201);
    expect(response.data.user).toBe(mockUser.username);
    expect(response.data.role).toBe('user');
    await logout();
  });

  it('user should be able to log in', async () => {
    const response = await login(mockUser.username, mockUser.password);
    expect(response.data.role).toBe('user');
    expect(response.data.username).toBe(mockUser.username);
    expect(response.status).toBe(200);
    await logout();
  });

  it('user should be able to edit profile', async () => {
    await login(mockUser.username, mockUser.password);
    const response = await axios.put(`${link}/edit`, {
      country: 'thailand',
    }, {
      withCredentials: true,
    });
    expect(response.status).toBe(200);
    await logout();
  });

  it('user can get their user', async () => {
    await login(mockUser.username, mockUser.password);
    const res = await axios.get(`${link}/user`, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    expect(res.status).not.toBe(400);
    expect(res.data.user.username).toBe(mockUser.username);
    await logout();
  });

  it('user can get their detail', async () => {
    await login(mockUser.username, mockUser.password);
    const res = await axios.get(`${link}/get/detail/${mockUser.username}`, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    expect(res.data.username).toBe(mockUser.username);
    await logout();
  });
  it('check same user', async () => {
    const res = await axios.get(`${link}/search/username/${mockUser.username}`);
    expect(res.status).toBe(200);
    expect(res.data.result).toBe('Same');

    const res2 = await axios.get(`${link}/search/username/${mockUser.username}${mockUser.username}`);
    expect(res2.status).toBe(200);
    expect(res2.data.result).toBe('Available');
    await logout();
  });
  it('check same email', async () => {
    const res = await axios.get(`${link}/search/email/${mockUser.email}`);
    expect(res.status).toBe(200);
    expect(res.data.result).toBe('Same');

    const res2 = await axios.get(`${link}/search/email/${mockUser.email}${mockUser.email}`);
    expect(res2.status).toBe(200);
    expect(res2.data.result).toBe('Available');
    await logout();
  });

  it('only admins can be get all users', async () => {
    await login('jest', 'jest');
    const res2 = await axios.get(`${link}/all`, {
      withCredentials: true,
    });
    expect(res2.status).toBe(200);
    await login(mockUser.username, mockUser.password);
    await logout();
  });

  it('user can log out', async () => {
    await login(mockUser.username, mockUser.password);
    const res = await axios.post(`${link}/logout`, {
      withCredentials: true,
    });
    expect(res.status).toBe(200);
    await logout();
  });
  it('admin can delete user', async () => {
    await login('jest', 'jest');
    const res2 = await axios.delete(`${link}/${mockUser.username}`, {
      withCredentials: true,
    });
    expect(res2.status).toBe(200);
    await logout();
  });
});
