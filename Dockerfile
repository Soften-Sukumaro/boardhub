FROM node:10

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install

COPY . .

WORKDIR /usr/src/app/client

RUN npm run build
WORKDIR /usr/src/app/client/dist

COPY . ../../Backend/dist

WORKDIR /usr/src/app/Backend
EXPOSE 3000

CMD ["npm","start"]