import { mapActions } from 'vuex';
import { getProductById } from '@/utils/utils';

export default {
  methods: {
    ...mapActions(['addCart', 'incrementCart']),
    addToCart(product) {
      let cart = JSON.parse(localStorage.getItem('cart')) || [];
      if (cart.length) {
        let item = getProductById(cart, product.product_id);

        if (item) {
          // check if is not new item
          this.incrementCart(item);
          localStorage.setItem('cart', JSON.stringify(cart));
        } else {
          this.updateToCart(cart, product);
        }
      } else {
        this.updateToCart(cart, product);
      }
    },
    updateToCart(cart, product) {
      // console.log('Update Cart')
      let item = {
        id: product.product_id,
        image: product.product_photo_link,
        name: product.product_name,
        price: product.product_price,
        subtotal: product.product_price * 1,
        qty: 1
      };
      cart.push(item);
      this.addCart(item);
      localStorage.setItem('cart', JSON.stringify(cart));
    }
  }
};
