import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

const ifAdmin = (to, from, next) => {
  if (store.getters.isAdmin) {
    next()
    return
  }
  next('/')
}

const ifCreator = (to, from, next) => {
  if (store.getters.isCreator) {
    next()
    return
  }
  next('/')
}

export default new Router({
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/Home')
    },
    {
      path: '/login',
      name: 'frontend-login',
      component: () => import('@/views/FrontendLogin'),
      beforeEnter: ifNotAuthenticated,
    },
    {
      path: '/register',
      name: 'frontend-register',
      component: () => import('@/views/FrontendRegister')
    },
    {
      path: '/user/profile/:username',
      name: 'user-profile',
      component: () => import('@/views/FrontendProfile')
    },
    {
      path: '/search',
      name: 'product-list',
      component: () => import('@/views/FrontendProductList')
    },
    {
      path: '/product/:id',
      name: 'product',
      component: () => import('@/views/ProductDetail'),
      props: true
    },
    {
      path: '/edit/product/:id',
      name: 'edit-product',
      component: () => import('@/views/BackendProductEdit'),
      props: true,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('@/views/Cart')
    },
    {
      path: '/order',
      name: 'user-order',
      component: () => import('@/views/FrontendOrderList'),
      beforeEnter: ifAuthenticated
    },
    {
      path: '/shipping',
      name: 'user-shipping',
      component: () => import('@/views/FrontendShipping'),
      beforeEnter: ifAuthenticated
    },
    {
      path: '/creator',
      name: 'creator',
      component: () => import('@/views/Creator'),
      beforeEnter: ifCreator,
      children: [
        {
          path: 'product',
          name: 'creator-product-list',
          component: () => import('@/components/CreatorProductList.vue')
        },
        {
          path: 'order',
          name: 'creator-order-list',
          component: () => import('@/components/CreatorOrderList.vue')
        },
      ]
    },
    {
      path: '/admin',
      name: 'backend',
      component: () => import('@/views/Backend'),
      beforeEnter: ifAdmin,
      children: [
        {
          path: 'product',
          name: 'admin-product-list',
          component: () => import('@/components/BackendProductList.vue')
        },
        {
          path: 'user',
          name: 'admin-user-list',
          component: () => import('@/components/BackendUserList.vue')
        },
        {
          path: 'order',
          name: 'admin-order-list',
          component: () => import('@/components/BackendOrderList.vue')
        },
      ]
    },
    {
      path: '*',
      redirect: { name: 'home' }
    }
  ]
})
