import axios from 'axios';

export default () => {
  return axios.create({
    baseURL: process.env.NODE_ENV === 'production' ? 'http://35.240.204.144:3000/api':'http://localhost:3000/api',
    /* baseURL: 'http://localhost:3000/api', */
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
    },
  });
};
