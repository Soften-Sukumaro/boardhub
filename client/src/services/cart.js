import Api from './api';

export default {
  getCart(id) {
    return Api().get(`orderProducts/get/byorder/${id}`)
  },
  addCart(params) {
    return Api().post(`orderProducts/addtocart`, params)
  },
  updateAmount(params) {
    return Api().post(`orderProducts/setamount`, params)
  },
  removeCart(id) {
    return Api().post(`orderProducts/delete`, id)
  },
  getCartById(id) {
    return Api().get(`orderProducts/get/cart/byuser/${id}`)
  },
  getCartByOrder(id) {
    return Api().get(`orderProducts/get/byorder/${id}`)
  }
};
