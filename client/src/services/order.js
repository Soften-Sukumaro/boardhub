import Api from './api';

export default {
  getOrder(id) {
    return Api().get(`orders/get/byuser/${id}`)
  },
  saveOrder(params) {
    return Api().post(`orders/checkout`, params)
  },
  getOrderList() {
    return Api().get(`orders/all`)
  },
  addConfirmation(params) {
    return Api().post(`confirmations/add`, params)
  },
  getInvoice(id) {
    return Api().get(`invoices/get/byorder/${id}`)
  },
  getConfirm(id) {
    return Api().get(`confirmations/get/byorder/${id}`)
  },
  confirm(params) {
    return Api().post(`confirmations/confirm`, params)
  },
  regisItem(params) {
    return Api().post(`shipments/regisitem`, params)
  },
  getShipment(id) {
    return Api().get(`shipments/get/receiver/byuser/${id}`)
  },
  confirmShipment(params) {
    return Api().post(`shipments/confirm`, params)
  },
  getByRef(id) {
    return Api().get(`orderProducts/get/byref/${id}`)
  }

}