import Api from './api';

export default {
  upload(id ,formData) {
    return Api().put(`/products/photos/${id}`, formData)
  },
  uploadProduct(formData) {
    return Api().post(`/products/photo`, formData)
  },
  uploadConfirm(formData) {
    return Api().post(`/confirmations/addphoto`, formData)
  },
  uploadProfile(formData) {
    return Api().put(`/users/profile`, formData)
  }
};
