import { LOGIN, LOGOUT, PURGE_AUTH, SET_AUTH, SET_ERROR, CHECK_AUTH } from './mutation-types'
import UserService from '@/services/auth'
import { SET_CART, LIST_CART } from '../cart/mutation-types';

export default {
  [LOGIN]: ({commit}, user) => {
    return new Promise((resolve, reject) => {
      UserService.login(user)
        .then(({data}) => {
          commit(SET_AUTH, data)
          resolve(data)
        })
        .catch(({response}) => {
          //console.log(response.data)
          commit(SET_ERROR, response.data)
        })
    })
  },
  [LOGOUT]: ({commit}) => {
    commit(PURGE_AUTH)
  },
  [CHECK_AUTH]: ({commit, dispatch}) => {
    if (window.localStorage.getItem('user')) {
      UserService.getUser()
        .then(({data}) => {
          commit(SET_AUTH, data.user)
          dispatch(LIST_CART, data.user.id)
        })
        .catch(({response}) => {
          commit(SET_ERROR, response.data.errors)
        })
    } else {
      commit(PURGE_AUTH)
    }
  }
};
