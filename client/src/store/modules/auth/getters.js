export default {
  isAuthenticated: state => state.isAuthenticated,
  isAdmin: state => state.isAdmin,
  isCreator: state => state.isCreator,
  currentUser: state => state.user,
  errorMsg: state => state.errors
};
