import getters from './getters';
import mutations from './mutations';
import actions from './actions';

const state = {
  errors: null,
  user: {},
  isAuthenticated: !!localStorage.getItem('user'),
  isAdmin: !!localStorage.getItem('admin'),
  isCreator: !!localStorage.getItem('creator')
};

export default {
  getters,
  state,
  mutations,
  actions
};
