import { SET_ERROR, SET_AUTH, PURGE_AUTH } from './mutation-types'
import { SET_CART } from '../cart/mutation-types';

export default {
  [SET_ERROR]: (state, error) => {
    state.errors = error
  },
  [SET_AUTH]: (state, user) => {
    state.isAuthenticated = true
    state.user = user
    state.errors = {}
    window.localStorage.setItem('user', true)
    if (user.role === 'admin') {
      window.localStorage.setItem('admin', true)
      state.isAdmin = true
    } else if (user.role === 'creator') {
      window.localStorage.setItem('creator', true)
      state.isCreator = true
    }
  },
  [PURGE_AUTH]: (state) => {
    state.isAuthenticated = false
    state.user = {}
    state.errors = {}
    /* this.$store.commit(SET_CART, []) */
    window.localStorage.removeItem('user')
    //if (window.localStorage.getItem('admin'))
    window.localStorage.removeItem('admin')
      //if (window.localStorage.getItem('creator'))
    window.localStorage.removeItem('creator')
  }
};
