export default {
  cart: state => state.cart,
  cart_order: state => state.cart_order
};
