import {
  SET_CART,
  SET_ORDER,
  SET_CART_ID,
  ADD_CART,
  SET_CART_ORDER,
  INCREMENT_CART,
  DECREMENT_CART,
  CHANGE_CART,
  PURGE_CART,
  UPDATE_CART,
} from './mutation-types';

export default {
  [ADD_CART](state, payload) {
    state.cart.push(payload);
  },
  [SET_CART_ORDER]: function (state, payload) {
    state.cart_order = payload
  },
  [SET_CART]: function(state, payload) {
    let data = []
    let item = {
      id: '',
      amount: '',
      createdAt: '',
      updatedAt: '',
      orderId: '',
      productId: '',
      refOrderProductStatusId: null,
      name: '',
      price: '',
      main_photo: ''
    }
    payload.forEach(function(el) {
      item = {
        id: el.id,
        amount: el.amount,
        createdAt: el.createdAt,
        updatedAt: el.updatedAt,
        orderId: el.orderId,
        productId: el.productId,
        refOrderProductStatusId: el.refOrderProductStatusId,
        name: el.product.name,
        price: el.product.price,
        main_photo: el.product.main_photo
      }
      data.push(item)
    })
    state.cart = data
  },
  [SET_ORDER] (state, order) {
    //console.log('order')
    state.orders = order
  },
  [SET_CART_ID] (state, cart_id) {
    //console.log('cart id'+cart_id)
    state.cart_id = cart_id
    window.localStorage.setItem('cart-id', cart_id)
    //console.log('xx' + window.localStorage.getItem('cart-id'))
  },
  [PURGE_CART] (state) {
    state.cart = []
  },
  [UPDATE_CART] (state, {item, amount}) {
    const record = state.cart.find(el => el.id === item.id)
    if (record) {
      if (isAdd) {
        record.amount += amount
      } else {
        record.amount = amount
      }
    } else {
      state.cart.push({
        ...item,
        amount
      })
    }
  }
};
