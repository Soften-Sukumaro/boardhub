import CommentService from '@/services/comment'
import { 
  ADD_COMMENT,
  GET_COMMENT,
  SET_COMMENT,
  DELETE_COMMENT,
 } from '../comment/mutation-types'

export default {
  [ADD_COMMENT]: ({commit}, params) => {
    return CommentService.addComment(params)
      .then(({data}) => {
      })
  },
  [GET_COMMENT]: ({commit}, id) => {
    return CommentService.getComment(id)
      .then(({data}) => {
        commit(SET_COMMENT, data)
      })
  },
  [DELETE_COMMENT]: ({commit}, id) => {
    return CommentService.deleteComment(id)
  }
}