export const ADD_COMMENT = 'ADD_COMMENT'
export const GET_COMMENT = 'GET_COMMENT'
export const SET_COMMENT = 'SET_COMMENT'
export const DELETE_COMMENT = 'DELETE_COMMENT'