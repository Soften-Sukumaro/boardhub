export default {
  cart_id: state => state.cart_id,
  orders: state => state.orders,
  invoice: state => state.invoice,
  confirm: state => state.confirm,
  shipments: state => state.shipments,
  ref2: state => state.ref2,
  ref3: state => state.ref3,
  ref4: state => state.ref4,
  ref5: state => state.ref5,
};
