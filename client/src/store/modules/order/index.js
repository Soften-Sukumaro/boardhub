import getters from './getters';
import mutations from './mutations';
import actions from './actions';

const state = {
  cart_id: '',
  orders: [],
  user_orders: [],
  invoice: '',
  confirm: '',
  shipments: [],
  ref2: [],
  ref3: [],
  ref4: [],
  ref5: [],
};

export default {
  state,
  getters,
  mutations,
  actions
};
