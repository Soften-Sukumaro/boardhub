import {
  SET_ORDER,
  SET_CART_ID,
  SET_INVOICE,
  SET_CONFIRM,
  SET_SHIPMENT,
  REF2,
  REF3,
  REF4,
  REF5,
} from './mutation-types';

export default {
  [SET_CART_ID] (state, cart_id) {
    state.cart_id = cart_id
  },
  [SET_ORDER] (state, orders) {
    state.orders = orders
  },
  [SET_INVOICE] (state, invoice) {
    state.invoice = invoice
  },
  [SET_CONFIRM] (state, confirm) {
    state.confirm = confirm
  },
  [SET_SHIPMENT] (state, shipments) {
    state.shipments = shipments
  },
  [REF2] (state, ref) {
    state.ref2 = ref
  },
  [REF3] (state, ref) {
    state.ref3 = ref
  },
  [REF4] (state, ref) {
    state.ref4 = ref
  },
  [REF5] (state, ref) {
    state.ref4 = ref
  },
};
