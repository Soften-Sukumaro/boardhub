export default {
  profile: state => state.profile,
  userList: state => state.userList,
  detail: state => state.detail
}