import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = { 
  errors: {}, 
  profile: {},
  userList: [],
  detail: {},

}

export default {
  getters,
  state,
  mutations,
  actions
}