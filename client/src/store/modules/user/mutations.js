import { SET_PROFILE, SET_USER_LIST, SET_DETAIL } from './mutation-type'

export default {
  [SET_PROFILE] (state, profile) {
    state.profile = profile
    state.errors = {}
  },
  [SET_USER_LIST] (state, userList) {
    state.userList = [].concat(userList)
  },
  [SET_DETAIL] (state, user) {
    state.detail = user
  }

  
}