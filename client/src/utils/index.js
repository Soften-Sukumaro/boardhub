import toastr from 'toastr'

toastr.options.positionClass = 'toast-top-center'

export const showMsg = message => {
    let content, type
    if (typeof message === 'string') {
        content = message
        type = 'error'
    } else {
        content = message.content
        type = message.type
    }
    toastr[type](content)
}