// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App'
import router from './router'
import store from './store';

import feather from 'vue-icon'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'

import { CHECK_AUTH } from './store/modules/auth/mutation-types'
import { GET_CART } from './store/modules/cart/mutation-types'

Vue.use(feather, 'v-icon')

window.toastr = require('toastr')
 
Vue.use(VueToastr2)

Vue.use(BootstrapVue)
Vue.config.productionTip = false

// Ensure we checked auth before each page load.
router.beforeEach(
  (to, from, next) => {
    return Promise
      .all([store.dispatch(CHECK_AUTH)])
      .then(() => {
        store.dispatch(GET_CART, {uid: store.getters.currentUser.id, currentCart: store.getters.cart})
      })
      .then(() => {
        /* store.dispatch(GET_CART_ID, store.getters.currentUser.id) */
        //store.dispatch(FETCH_ORDER, store.getters.currentUser.id)
      })
      .then(next)
  }
)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
