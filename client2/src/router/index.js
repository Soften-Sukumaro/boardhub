import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/Home')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('@/views/Test')
    },
    {
      path: '/login',
      name: 'frontend-login',
      component: () => import('@/views/FrontendLogin')
    },
    {
      path: '/register',
      name: 'frontend-register',
      component: () => import('@/views/FrontendRegister')
    },
    {
      path: '/user/profile/:username',
      name: 'user-profile',
      component: () => import('@/views/FrontendProfile')
    },
    {
      path: '/product/:id',
      name: 'product',
      component: () => import('@/views/ProductDetail'),
      props: true
    },
    {
      path: '/edit/product/:id',
      name: 'edit-product',
      component: () => import('@/views/BackendProductEdit'),
      props: true
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('@/views/Cart')
    },
    {
      path: '/order',
      name: 'user-order',
      component: () => import('@/views/FrontendOrderList')
    },
    {
      path: '/shipping',
      name: 'user-shipping',
      component: () => import('@/views/FrontendShipping')
    },
    {
      path: '/admin',
      name: 'backend',
      component: () => import('@/views/Backend'),
      children: [
        {
          path: 'product',
          name: 'admin-product-list',
          component: () => import('@/components/BackendProductList.vue')
        },
        {
          path: 'user',
          name: 'admin-user-list',
          component: () => import('@/components/BackendUserList.vue')
        },
        {
          path: 'order',
          name: 'admin-order-list',
          component: () => import('@/components/BackendOrderList.vue')
        },
      ]
    }
  ]
})
