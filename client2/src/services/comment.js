import Api from './api';

export default {
  addComment(params) {
    return Api().post(`/comments/`, params)
  },
  getComment(id) {
    return Api().get(`/comments/${id}`)
  },
  deleteComment(id) {
    return Api().delete(`/comments/${id}`)
  }
}