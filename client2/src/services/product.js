import Api from './api';

export default {
  getProducts() {
    return Api().get('/products/all');
  },

  getProductsById(id) {
    return Api().get(`/products/${id}`);
  },

  getProductsRelatedById(id) {
    return Api().get(`/products/${id}/related`);
  },

  getProductsByPage(page) {
    return Api().get(`/products?page=${page}`);
  },

  create(params) {
    return Api().post(`/products/add`, params)
  },

  update(id, params) {
    return Api().put(`/products/${id}`, params)
  },
  destroy(id) {
    return Api().delete(`/products/${id}`)
  }
};
