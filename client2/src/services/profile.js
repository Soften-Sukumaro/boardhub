import Api from './api';

export default {
  fetch (username) {
    return Api().get(`/users/get/detail/${username}`)
  },
  update(params) {
    return Api().put(`/users/edit`, params)
  },
  getList() {
    return Api().get(`users/all`)
  },
  destroy(username) {
    return Api().delete(`users/${username}`)
  },
  getDetail(id) {
    return Api().get(`/users/get/detailwoauthor/${id}`)
  }
}