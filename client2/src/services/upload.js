import Api from './api';

export default {
  upload(id ,formData) {
    return Api().put(`/products/photos/${id}`, formData)
  },
  uploadProduct(formData) {
    return Api().post(`/products/photo`, formData)
  }
};
