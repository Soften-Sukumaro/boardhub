import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import cart from './modules/cart'
import user from './modules/user'
import product from './modules/product'
import order from './modules/order'
import comment from './modules/comment'

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: { 
    auth, 
    cart, 
    user,
    product,
    order,
    comment }
});

export default store;
