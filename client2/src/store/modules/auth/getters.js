export default {
  isAuthenticated: state => state.isAuthenticated,
  isAdmin: state => state.isAdmin,
  currentUser: state => state.user,
  errorMsg: state => state.errors
};
