import {
  GET_CART,
  GET_ORDER_CART,
  FETCH_CART,
  SET_CART,
  SET_CART_ORDER,
  CHECK_CART,
  ADD_CART,
  LIST_CART,
  CLEAR_CART,
  PURGE_CART,
  UPDATE_CART,
  UPDATE_AMOUNT,
  DELETE_ITEM,
} from './mutation-types';
import CartService from '@/services/cart'

export default {
  [GET_CART]: function ({commit}, {uid, currentCart}) {
    if (uid) {
      return CartService.getCartById(uid)
        .then(({data}) => {
          if(data && (!currentCart || currentCart.length === 0)) {
            commit('SET_CART', data)
          }
        })
    }
  },
  [GET_ORDER_CART]: function ({commit}, id) {
    return CartService.getCartByOrder(id)
      .then(({data}) => {
        commit(SET_CART_ORDER, data.rows)
      })
  },
  [CHECK_CART]: ({dispatch}) => {
    if (window.localStorage.getItem('user')) {
      //console.log(state)
      let cart_id =  window.localStorage.getItem('cart-id')
      //console.log('yy' + cart_id)
      dispatch(FETCH_CART, cart_id)
    } else {
      window.localStorage.removeItem('cart-id')
    }
  },
  [ADD_CART]: function ({commit}, {item, isAdd}) {
    return new Promise((resolve, reject) => {
      CartService.addCart({productId: item.id})
      .then(() => {
        commit(UPDATE_CART, {item})
      })
    })
  },  
  [LIST_CART]: function ({commit}, product) {
    return CartService.getCartById(product)
      .then(({data}) => {
        commit(SET_CART, data.rows)
      })
  },
  [CLEAR_CART]: function ({commit}) {
    commit(PURGE_CART)
  },
  [UPDATE_AMOUNT]: function ({commit}, item) {
    return CartService.updateAmount(item)
  },
  [DELETE_ITEM]: function ({commit}, id) {
    return CartService.removeCart(id)
  }
}

