import getters from './getters';
import mutations from './mutations';
import actions from './actions';

const state = {
  cart: [],
  cart_order: [],
  orders: [],
};

export default {
  state,
  getters,
  mutations,
  actions
};
