import getters from './getters';
import mutations from './mutations';
import actions from './actions';

const state = {
  comments: []
};

export default {
  getters,
  state,
  mutations,
  actions
};
