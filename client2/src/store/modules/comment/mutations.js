import { SET_COMMENT } from './mutation-types'

export default {
  [SET_COMMENT]: (state, comments) => {
    state.comments = comments
  }
};
