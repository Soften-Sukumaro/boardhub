import {
  FETCH_ORDER,
  FETCH_USER_ORDER,
  FETCH_ORDER_LIST,
  ADD_COMFIRM,
  CONFIRM,
  SET_ORDER,
  SET_INVOICE,
  GET_INVOICE,
  GET_COMFIRM,
  SET_CONFIRM,
  CHECKOUT,
  GET_CART_ID,
  SET_CART_ID,
  REGIS_ITEM,
  GET_SHIPMENT,
  SET_SHIPMENT,
  CONFIRM_SHIPMENT,
} from './mutation-types';
import OrderService from '@/services/order'

export default {
  [FETCH_ORDER]: function ({commit}, id) {
    return OrderService.getOrder(id)
    .then(({data}) => {
      const orders = data.filter( a => {
        return a.refOrderStatusId === 2 || a.refOrderStatusId === 3
      })
      commit(SET_ORDER, orders)
    })
  },
  [FETCH_USER_ORDER]: function ({commit}, id) {
    return OrderService.getOrder(id)
      .then(({data}) => {})
  },
  [CHECKOUT]: function ({commit}, order) {
    console.log(order)
    return OrderService.saveOrder(order)
      .then(({data}) => {
      })
  },
  [FETCH_ORDER_LIST]: function ({commit}) {
    return OrderService.getOrderList()
      .then(({data}) => {
        const orders = data.filter( a => {
          return a.refOrderStatusId === 2 || a.refOrderStatusId === 3
        })
        commit(SET_ORDER, orders)
      })
  },
  [GET_INVOICE]: function ({commit}, id) {
    return OrderService.getInvoice(id)
      .then(({data}) => {
        commit(SET_INVOICE, data)
      })
  },
  [ADD_COMFIRM]: function ({commit}, formData) {
    return OrderService.addConfirmation(formData)
  },
  [GET_COMFIRM]: function ({commit}, id) {
    return OrderService.getConfirm(id)
      .then(({data}) => {
        commit(SET_CONFIRM, data)
      })
  },
  [CONFIRM]: function ({commit}, params) {
    return OrderService.confirm(params)
      .then(({data}) => {
        
        console.log(data)
      })
  },
  [GET_CART_ID]: function ({commit}, id) {
    return OrderService.getOrder(id)
      .then(({data}) => {
        const cart = data.filter( a => {
          return a.refOrderStatusId === 1
        })
        console.log(cart[0])
        commit(SET_CART_ID, cart[0].id)
        //commit(SET_ORDER, data)
        
      })
  },
  [REGIS_ITEM]: function ({commit}, params) {
    return OrderService.regisItem(params)
      .then(() => {
      })
  },
  [GET_SHIPMENT]: function ({commit}, id) {
    return OrderService.getShipment(id)
      .then(({data}) => {
        commit(SET_SHIPMENT, data)
      })
  },
  [CONFIRM_SHIPMENT]: function ({commit}, params) {
    return OrderService.confirmShipment(params)
  }
}

