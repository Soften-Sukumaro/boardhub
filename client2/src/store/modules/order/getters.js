export default {
  cart_id: state => state.cart_id,
  orders: state => state.orders,
  invoice: state => state.invoice,
  confirm: state => state.confirm,
  shipments: state => state.shipments
};
