import getters from './getters';
import mutations from './mutations';
import actions from './actions';

const state = {
  cart_id: '',
  orders: [],
  user_orders: [],
  invoice: '',
  confirm: '',
  shipments: [],
};

export default {
  state,
  getters,
  mutations,
  actions
};
