import {
  SET_ORDER,
  SET_CART_ID,
  SET_INVOICE,
  SET_CONFIRM,
  SET_SHIPMENT,
} from './mutation-types';

export default {
  [SET_CART_ID] (state, cart_id) {
    state.cart_id = cart_id
  },
  [SET_ORDER] (state, orders) {
    state.orders = orders
  },
  [SET_INVOICE] (state, invoice) {
    state.invoice = invoice
  },
  [SET_CONFIRM] (state, confirm) {
    state.confirm = confirm
  },
  [SET_SHIPMENT] (state, shipments) {
    state.shipments = shipments
  }
};
