import {
  FETCH_PRODUCT,
  FETCH_PRODUCT_LIST,
  PUBLISH_PRODUCT,
  EDIT_PRODUCT,
  DELETE_PRODUCT,
} from './mutation-types';
import ProductService from '@/services/product'

export default {
  [FETCH_PRODUCT]: ({commit}, id) => {
    return ProductService.getProductsById(id)
      .then((res) => {
        commit(FETCH_PRODUCT, res.data)
      })
  },
  [FETCH_PRODUCT_LIST]: ({commit}) => {
    return ProductService.getProducts()
      .then(({data}) => {
        commit(FETCH_PRODUCT_LIST, data)
      })
  },
  [PUBLISH_PRODUCT]: ({ state }, product) => {
    return ProductService.create(product)
  },
  [EDIT_PRODUCT]: ({ state }, product) => {
    return ProductService.update(product.id, product)
  },
  [DELETE_PRODUCT]: ({state}, id) => {
    return ProductService.destroy(id)
  },


}