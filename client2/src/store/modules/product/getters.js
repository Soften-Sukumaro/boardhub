export default {
  product: state => state.product,
  products: state => state.products
};
