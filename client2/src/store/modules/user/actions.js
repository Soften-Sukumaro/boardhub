import ProfileService from '@/services/profile'
import { FETCH_PROFILE, 
  FETCH_USER_LIST, 
  EDIT_PROFILE, 
  SET_PROFILE,
  SET_USER_LIST, 
  DELETE_USER,
  GET_DETAIL,
  SET_DETAIL
} from './mutation-type'

export default {
  [FETCH_PROFILE]: function ({commit}, username) {
    const user = username
    //console.log(user)
    return ProfileService.fetch(user)
      .then(({data}) => {
        //console.log(data)
        commit(SET_PROFILE, data)
        return data
      })
      .catch(({response}) => {

      })
  },
  [FETCH_USER_LIST]: function ({commit}) {
    return ProfileService.getList()
      .then(({data}) => {
        commit(SET_USER_LIST, data)
      })
  },
  [EDIT_PROFILE]: function ({state}, profile) {
    return ProfileService.update(profile)
  },
  [DELETE_USER]: function (state, user) {
    return ProfileService.destroy(user)
  },
  [GET_DETAIL]: function ({commit}, id) {
    return ProfileService.getDetail(id) 
      .then(({data}) => {
        commit(SET_DETAIL, data)
      })
  }

}