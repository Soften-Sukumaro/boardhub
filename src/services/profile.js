import Api from './api';

export default {

  fetch (params) {
    let config = {
      headers: {
        Authorization: 'Bearer '+params
      }
    }
    return Api().get(`/users/get/detail/me`, config)
  }
}