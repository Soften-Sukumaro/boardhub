import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import cart from './modules/cart';
import user from './modules/user'

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: { 
    auth, 
    cart, 
    user }
});

export default store;
