import { LOGIN, LOGOUT } from './mutation-types';
import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT } from './mutation-types'
import { USER_REQUEST } from '../user/mutation-type'
import UserService from '@/services/user';

export default {
  [AUTH_REQUEST]: ({commit, dispatch}, user) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST)
      UserService.login(user)
        .then(res => {
          localStorage.setItem('user-token', res.data.token)
          commit(AUTH_SUCCESS, res.data)
          dispatch(USER_REQUEST, res.data.token)
          resolve(res)
        })
        .catch(err => {
          commit(AUTH_ERROR, err)
          localStorage.removeItem('user-token')
          reject(err)
        })
    })
  },
  [AUTH_LOGOUT]: ({commit, dispatch}) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_LOGOUT)
      localStorage.removeItem('user-token')
      resolve()
    })
  }
  /* logIn({ commit, rootState }) {
    commit(LOGIN, rootState.auth);
  },
  logOut({ commit, rootState }) {
    commit(LOGOUT, rootState.auth);
  } */
};
