export default {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status
  /* getLoggedIn: state => state.loggedIn,
  getToken: state => state.token,
  getUsername: state => state.username */
};
