import getters from './getters';
import mutations from './mutations';
import actions from './actions';

const state = {
  token: localStorage.getItem('user-token') || '',
  status: '',
  hasLoadedOnce: false
  /* loggedIn: localStorage.getItem('loggedIn') || false,
  token: localStorage.getItem('token') || "",
  username : localStorage.getItem('username') || "" */
};

export default {
  getters,
  state,
  mutations,
  actions
};
