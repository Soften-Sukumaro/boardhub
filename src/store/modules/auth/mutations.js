import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT } from './mutation-types'

export default {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [AUTH_SUCCESS]: (state, res) => {
    state.status = 'success'
    state.token = res.token
    state.hasLoadedOnce = true
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error'
    state.hasLoadedOnce = true
  },
  [AUTH_LOGOUT]: (state) => {
    state.token = ''
  }
  /* [LOGIN](state, payload) {
    payload.loggedIn = true;
  },

  [LOGOUT](state, payload) {
    payload.loggedIn = false;
  } */
};
