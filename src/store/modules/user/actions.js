import { mapState } from 'vuex';
import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from './mutation-type'
import { AUTH_LOGOUT } from '../auth/mutation-types'
import ProfileService from '@/services/profile'

export default {
  [USER_REQUEST]: ({state ,commit, dispatch}, token) => {
    commit(USER_REQUEST)
    //console.log(token)
    ProfileService.fetch(token)
      .then(res => {
        commit(USER_SUCCESS, res.data[0])
      })
      .catch(res => {
        commit(USER_ERROR)
        // if resp is unauthorized, logout, to
        dispatch(AUTH_LOGOUT)
      })
  },
}