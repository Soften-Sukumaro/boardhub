import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = { 
  status: '', 
  profile: {} 
}

export default {
  getters,
  state,
  mutations,
  actions
}