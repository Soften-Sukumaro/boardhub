import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from './mutation-type'
import { AUTH_LOGOUT } from '../auth/mutation-types'
import Vue from 'vue'

export default {
  [USER_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [USER_SUCCESS]: (state, payload) => {
    state.status = 'success'
    Vue.set(state, 'profile', payload)
  },
  [USER_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.profile = {}
  }
}